// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery
//= require_tree .
//= require_self

if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}
$(function() {
	$(".action-btn").click(function(e) {
		var link = $(this).attr("href");
		var title = $(this).attr("title");
		e.preventDefault();
		bootbox.confirm(title + "?", function(result) {
			if (result) {
				document.location.href = link;
			}
		});
	});
});