function showCourseIdDialog() {
	bootbox.dialog({
		title:"Add a Course",
		message:"<div class='row'><div class='col-md-12'>" +
				"<div class='form-group col-md-9'>" +
				"<label for='courseIdField'>Course Idenitifer</label>" +
				"<input type='text' class='form-control' id='courseIdField'>" +
				"<p id='validationMessage' class='text-muted'></p>" +
				"</div></div></div>",
		buttons: {
			success: {
				label: "Continue",
				className: "btn btn-success",
				callback: function() {
					showCourseTitleDialog();
				}
			}
		}
	});
	$("#courseIdField").bind("input", function() {
		hisp.courseId = $("#courseIdField").val();
		var validationLink = hisp.validationLink;
		var year = $("#year").val();
		var semester = $("#semester").val();
		$.getJSON(validationLink+"/"+year+"/"+hisp.courseId, validateCourse);
	});
}

function validateCourse(data) {
	if (hisp.courseId != $("#courseIdField").val())
		return; // outdated information
	if (data.seenThisYear == true) {
		$("#validationMessage").html("Course "+hisp.courseId+" was found in our database.<br>Press continue to confirm.");
		hisp.seen = "thisYear";

	} else if (data.seenPrevYears == true) {
		$("#validationMessage").html(
			"Course "+hisp.courseId+" was found for a previous year.<br>" + 
			"Press continue to confirm.");
		hisp.seen = "prevYear";
	} else {
		$("#validationMessage").html(
			"Course "+hisp.courseId+" was not found in our database.<br>" + 
			"Press continue to manually enter the course information.");
		hisp.seen = "never";
	}
	hisp.courseTitle = data.courseTitle;
}

function showCourseTitleDialog() {
	var message = "<div class='row'><div class='col-md-12'>" +
				"<div class='form-group col-md-9'>" +
				"<label for='courseTitleField'>Course Name</label>";
	switch(hisp.seen) {
		case "thisYear":
			message += "<input type='text' class='form-control' id='courseTitleField' value='"+hisp.courseTitle+"' disabled=true>" +
				"<p id='validationMessage' class='text-muted'></p>" +
				"</div></div></div>";
			break;
		case "prevYear":
			message += "<input type='text' class='form-control' id='courseTitleField' value='"+hisp.courseTitle+"'>" +
				"<p id='validationMessage' class='text-muted'>Please edit the title if necessary</p>" +
				"</div><</div></div>";
			break;
		case "never":
			message += "<input type='text' class='form-control' id='courseTitleField'>" +
				"<p id='validationMessage' class='text-muted'>Please add a title if necessary</p>" +
				"</div></div></div>";
			break;
		default:
			return;
	}
	bootbox.dialog({
		title:"Add a Course",
		message:message,
		buttons: {
			success: {
				label: "Continue",
				className: "btn btn-success",
				callback: function() {
					updateCourseTitle();
					showScheduleBlockDialog();
				}
			}
		}
	});
	$("#courseTitleField").bind("input", function() {
		hisp.courseTitle = $("#courseTitleField").val();
	});
}

function updateCourseTitle() {
	$.ajax({
		url: hisp.updateCourseTitleLink,
		data: {year: $("#year").val(),
			courseId: hisp.courseId,
			courseTitle: hisp.courseTitle},
		type: "POST",
		error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(jqXHR.responseText);
		}
	});
}

function showScheduleBlockDialog() {
	bootbox.dialog({
		title:"Add a Course",
		message:"<div class='row'><div class='col-md-12'>" +
				"<div class='form-group col-md-9'>" +
				$("#hidden").html() +
				"<p id='validationMessage' class='text-muted'></p>" +
				"</div></div></div>",
		buttons: {
			success: {
				label: "Continue",
				className: "btn btn-success",
				callback: function() {
					validateScheduleBlock();
				}
			}
		}
	});
}

function validateScheduleBlock() {
	hisp.semesterStart = $(".bootbox-body #semStartField").val()
	hisp.semesterEnd = $(".bootbox-body #semEndField").val()
	hisp.periods = $(".bootbox-body #periodField").val().join(',');
	hisp.days = $(".bootbox-body #dayField").val().join(',');
	$.ajax({
		url: hisp.validateScheduleBlockLink,
		data: {year: $("#year").val(),
			semesterStart: hisp.semesterStart,
			semesterEnd: hisp.semesterEnd,
			days: hisp.days,
			periods: hisp.periods,
			courseId: hisp.courseId},
		async: true,
		type: "POST",
		success: function(data, textStatus, xhr) {
			if (!data.valid)
				bootbox.alert("Sorry, that is not a legal schedule block");
			else {
				hisp.teacher = data.teacher;
				hisp.sectionSeen = data.sectionSeen;
				showTeacherDialog();
			}
		},
		error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(jqXHR.responseText);
		}
	});
}

function showTeacherDialog() {
	var message = "<div class='row'><div class='col-md-12'>" +
		"<div class='form-group col-md-9'>" +
		"<label for='teacherField'>Teacher Name</label>";
	if(hisp.sectionSeen) {
		message += "<input type='text' class='form-control' id='teacherField' value='"+hisp.teacher+"' disabled=true>";
	} else {
		message += "<input type='text' class='form-control' id='teacherField'>";
	}
	message += "<p id='validationMessage' class='text-muted'></p>" +
		"</div></div></div>";
	bootbox.dialog({
		title:"Add a Course",
		message:message,
		buttons: {
			success: {
				label: "Finish",
				className: "btn btn-success",
				callback: function() {
					hisp.oldTeacher = hisp.teacher;
					hisp.teacher = $("#teacherField").val();
					updateTeacher();
					addCourse();
				}
			}
		}
	});
}

function updateTeacher() {
	$.ajax({
		url: hisp.updateTeacherLink,
		data: {year: $("#year").val(),
			semesterStart: hisp.semesterStart,
			semesterEnd: hisp.semesterEnd,
			days: hisp.days,
			periods: hisp.periods,
			courseId: hisp.courseId,
			teacher: hisp.teacher},
		async:   false,
		type: "POST",
		success: function(data, textStatus, xhr) {
			if (data == "Teacher not updated" && hisp.oldTeacher != hisp.teacher) {
				$("#scheduleSelector").before("<div class='alert alert-danger' role='status'>"+
					"Sorry, could not update the teacher's name<br>"+
					"Looks like someone already entered the teacher's name.</div>");
			}
		},
		error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(jqXHR.responseText);
		}
	});
}

function addCourse() {
	$.ajax({
		url: hisp.addCourseLink,
		data: {year: $("#year").val(),
			semesterStart: hisp.semesterStart,
			semesterEnd: hisp.semesterEnd,
			days: hisp.days,
			periods: hisp.periods,
			courseId: hisp.courseId,
			teacher: hisp.teacher},
		async: true,
		type: "POST",
		success: function(data, textStatus, xhr) {
			bootbox.alert("Course added successfully");
			$("#findScheduleButton").click(); // update schedule
		},
		error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(jqXHR.responseText);
		}
	});
}

function getSchedule() {
	$.ajax({
		url: hisp.getScheduleLink,
		type: "POST",
		data: {year : $("#year").val(),
		semester : $("#semester").val(),
		showFriends: $("#showHideFriends").prop('checked')},
		success: function(data, textStatus, xhr) {
			$("#addCourseBtn").show()
			$("#scheduleContainer").html(data);
		},
		error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(jqXHR.responseText);
		}
	});	
}