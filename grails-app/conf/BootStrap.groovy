import com.administration.AcademicYearScheduleBlockSet
import com.administration.Administrator
import com.administration.ScheduleBlock
import com.administration.School
import com.schedule.Course
import com.schedule.CourseSection
import com.schedule.DesiredScheduleCourse
import com.schedule.DesiredScheduleCriteria
import com.schedule.Schedule;
import com.security.AccountRequest;
import com.security.SecRole;
import com.security.User
import com.security.UserSecRole;
import com.student.Student

import grails.plugin.springsecurity.SecurityFilterPosition;
import grails.plugin.springsecurity.SpringSecurityUtils;
import grails.util.Environment

class BootStrap {
	 def concurrentSessionController 
    def securityContextPersistenceFilter 
	def init = { servletContext ->
		securityContextPersistenceFilter.forceEagerSessionCreation = true 
        SpringSecurityUtils.clientRegisterFilter('concurrentSessionFilter', SecurityFilterPosition.CONCURRENT_SESSION_FILTER) 
		def adminRole = new SecRole(authority: 'ROLE_ADMIN').save(flush: true)
		def studentRole = new SecRole(authority: 'ROLE_STUDENT').save(flush: true)

		if (Environment.current == Environment.DEVELOPMENT) {
			def school1=new School(name:"XYZ School",numSemesters:4,numDays:2, numPeriods:9,lunchPeriodStart:4,lunchPeriodEnd:7)
			def testYear=new AcademicYearScheduleBlockSet(school:school1,year:2015)
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'1,2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'1'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'1,2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'1'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'1,2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'1'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'4',days:'1,2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'4',days:'1'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'4',days:'2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1,2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'6',days:'1,2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'6',days:'1'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'6',days:'2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'7',days:'1,2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'7',days:'1'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'7',days:'2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'8',days:'1,2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'8',days:'1'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'8',days:'2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'9',days:'1,2'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'9',days:'1'))
			testYear.addToScheduleBlocks(new ScheduleBlock(periods:'9',days:'2'))
			school1.addToAcademicYearSchedules(testYear)
			def testYear1_2=new AcademicYearScheduleBlockSet(school:school1,year:2014)
			testYear1_2.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'1,2'))
			testYear1_2.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'1,2'))
			testYear1_2.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'1,2'))
			testYear1_2.addToScheduleBlocks(new ScheduleBlock(periods:'4',days:'1,2'))
			testYear1_2.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1,2'))
			school1.addToAcademicYearSchedules(testYear1_2)
			school1.save(failOnError:true)

			def school2=new School(name:"ABC School",numSemesters:4,numDays:2, numPeriods:6,lunchPeriodStart:3,lunchPeriodEnd:4).save(failOnError:true);
			def testYear2=new AcademicYearScheduleBlockSet(school:school2,year:2014)
			testYear2.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'1'))
			testYear2.addToScheduleBlocks(new ScheduleBlock(periods:'1,2',days:'1,2'))
			testYear2.addToScheduleBlocks(new ScheduleBlock(periods:'2,3',days:'1,2'))
			testYear2.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'1,2'))
			testYear2.addToScheduleBlocks(new ScheduleBlock(periods:'4',days:'1,2'))
			testYear2.addToScheduleBlocks(new ScheduleBlock(periods:'4,5',days:'2'))
			testYear2.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1'))
			testYear2.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1,2'))
			testYear2.addToScheduleBlocks(new ScheduleBlock(periods:'4,5',days:'1'))
			school2.addToAcademicYearSchedules(testYear2)
			def testYear3=new AcademicYearScheduleBlockSet(school:school2,year:2015)
			testYear3.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'1'))
			testYear3.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'1,2'))
			testYear3.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'1,2'))
			testYear3.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'1,2'))
			testYear3.addToScheduleBlocks(new ScheduleBlock(periods:'4',days:'1,2'))
			testYear3.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'2'))
			testYear3.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1'))
			testYear3.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1,2'))
			testYear3.addToScheduleBlocks(new ScheduleBlock(periods:'4,5',days:'1'))
			school2.addToAcademicYearSchedules(testYear3)
			school2.save(failOnError:true)
			def testUser = new Administrator(username: 'admin', password: '1234')
			testUser.save(failOnError: true)
			def admin2 = new Administrator(username: 'admin2', password: '1234')
			admin2.save(failOnError: true)			
			
			def student=new Student(email:"Bsmi@gmail.com",firstName:"Bob",lastName:"Smith",username:"student",password:"1234",school:school2)
			student.save(failOnError:true);
			def student2=new Student(email:"sdgsdg@gmail.com",firstName:"Tom",lastName:"Jackson",username:"student2",password:"1234",school:school2).save(failOnError:true)
			def student3=new Student(email:"eyery@gmail.com",firstName:"Adam",lastName:"Cobin",username:"student3",password:"1234",school:school1).save(failOnError:true)
			def student4=new Student(email:"eryery@gmail.com",firstName:"Terry",lastName:"Renner",username:"student4",password:"1234",school:school1).save(failOnError:true)
			def student5=new Student(email:"ututut@gmail.com",firstName:"John",lastName:"Whales",username:"student5",password:"1234",school:school2).save(failOnError:true)
			student.addToFriends(student2)
			student.addToFriends(student3)
			student2.addToFriends(student)
			student3.addToFriends(student)
			student2.save(failOnError:true)
			student3.save(failOnError:true)
			student.addToFriendRequests(student4)
			student.addToFriendRequests(student5)
			student.save(failOnError:true)
			UserSecRole.create testUser, adminRole, true
			UserSecRole.create admin2, adminRole, true
			UserSecRole.create student,studentRole,true
			UserSecRole.create student2,studentRole,true
			UserSecRole.create student3,studentRole,true
			UserSecRole.create student4,studentRole,true
			UserSecRole.create student5,studentRole,true

			Schedule studentSchedule1=new Schedule(year:2015,semester:1)
			def scheduleBlockArray=testYear3.scheduleBlocks.toArray().sort{
				[it.periods, it.days]
			}
			def c308=new Course(identity:"CSE308",title:"Software Engineering",school:school2)
			def c336=new Course(identity:"CSE336",title:"Internet Programming",school:school2)
			def c373=new Course(identity:"CSE373",title:"Algorithms",school:school2)
			def c114=new Course(identity:"CSE114",title:"Computer Science I",school:school2)
			def c320=new Course(identity:"CSE320",title:"System Fundamentals II",school:school2)
			c308.save(failOnError:true)
			c336.save(failOnError:true)
			c373.save(failOnError:true)
			c114.save(failOnError:true)
			c320.save(failOnError:true)
			def cs308=new CourseSection(teacher:"Scott Stoller",semesterStart:1,semesterEnd:1,course:c308,block:scheduleBlockArray[3])
			def cs336=new CourseSection(teacher:"Rob Kelly",semesterStart:1,semesterEnd:1,course:c336,block:scheduleBlockArray[4])
			def cs373=new CourseSection(teacher:"Rob Johnson",semesterStart:1,semesterEnd:1,course:c373,block:scheduleBlockArray[5])
			def cs114=new CourseSection(teacher:"Paul Fodor",semesterStart:1,semesterEnd:1,course:c114,block:scheduleBlockArray[6])
			def cs320=new CourseSection(teacher:"Jennifer Wong",semesterStart:1,semesterEnd:1,course:c320,block:scheduleBlockArray[2])
			studentSchedule1.addToCourses(cs308)
			studentSchedule1.addToCourses(cs336)
			studentSchedule1.addToCourses(cs373)
			studentSchedule1.addToCourses(cs114)
			studentSchedule1.addToCourses(cs320)
			student.addToAssignedSchedules(studentSchedule1)
			studentSchedule1.save(failOnError:true)
			student.save(failOnError:true)
			c308.save(failOnError:true)
			c336.save(failOnError:true)
			c373.save(failOnError:true)
			c114.save(failOnError:true)
			c320.save(failOnError:true)

			def cse114=new CourseSection(teacher:"Richard McKenna",semesterStart:1,semesterEnd:1,
				course:c114,block:scheduleBlockArray[7]).save(failOnError:true)
			new CourseSection(teacher:"Larry Wittie",semesterStart:1,semesterEnd:1,
				course:c320,block:scheduleBlockArray[8]).save(failOnError:true)


			Schedule studentSchedule3=new Schedule(year:2015,semester:1)
			def section114=new CourseSection(teacher:"Howard Foster",semesterStart:1,semesterEnd:1,course:c114,block:scheduleBlockArray[1]).save(failOnError:true)
			studentSchedule3.addToCourses(new CourseSection(teacher:"Scott Stoller",semesterStart:1,semesterEnd:1,course:c308,block:scheduleBlockArray[5]))
			studentSchedule3.addToCourses(new CourseSection(teacher:"Annie Wu",semesterStart:1,semesterEnd:1,course:c336,block:scheduleBlockArray[3]))
			studentSchedule3.addToCourses(new CourseSection(teacher:"Jon Jovi",semesterStart:1,semesterEnd:1,course:c373,block:scheduleBlockArray[2]))
			studentSchedule3.addToCourses(section114)
			student2.addToAssignedSchedules(studentSchedule3)
			student2.save(failOnError:true)
			
			
			Schedule studentSchedule5=new Schedule(year:2015,semester:1)
			studentSchedule5.addToCourses(section114)
			student5.addToAssignedSchedules(studentSchedule5)
			student5.save(failOnError:true)

			///school2 2014
			scheduleBlockArray=testYear2.scheduleBlocks.toArray().sort{
				[it.periods, it.days]
			}
			new CourseSection(teacher:"Scott Stoller",semesterStart:1,semesterEnd:1,course:c308,block:scheduleBlockArray[3]).save(failOnError:true)
			new CourseSection(teacher:"Rob Kelly",semesterStart:1,semesterEnd:1,course:c336,block:scheduleBlockArray[4]).save(failOnError:true)
			new CourseSection(teacher:"Rob Johnson",semesterStart:1,semesterEnd:1,course:c373,block:scheduleBlockArray[5]).save(failOnError:true)
			new CourseSection(teacher:"Paul Fodor",semesterStart:1,semesterEnd:1,course:c114,block:scheduleBlockArray[6]).save(failOnError:true)
			new CourseSection(teacher:"Jennifer Wong",semesterStart:1,semesterEnd:1,course:c320,block:scheduleBlockArray[2]).save(failOnError:true)
			new CourseSection(teacher:"Howard Foster",semesterStart:1,semesterEnd:1,course:c114,block:scheduleBlockArray[1]).save(failOnError:true)
			new CourseSection(teacher:"Scott Stoller",semesterStart:1,semesterEnd:1,course:c308,block:scheduleBlockArray[5]).save(failOnError:true)
			new CourseSection(teacher:"Annie Wu",semesterStart:1,semesterEnd:1,course:c336,block:scheduleBlockArray[3]).save(failOnError:true)
			new CourseSection(teacher:"Jon Jovi",semesterStart:1,semesterEnd:1,course:c373,block:scheduleBlockArray[2]).save(failOnError:true)
			
			
			
			
			
			
			Schedule studentSchedule2=new Schedule(year:2014,semester:2)
			scheduleBlockArray=testYear1_2.scheduleBlocks.toArray().sort{
				[it.periods, it.days]
			}
			studentSchedule2.addToCourses(new CourseSection(teacher:"James Xu",semesterStart:2,semesterEnd:2,course:new Course(identity:"AMS300",title:"Graph Theory",school:school1).save(failOnError:true),block:scheduleBlockArray[0]))
			studentSchedule2.addToCourses(new CourseSection(teacher:"Estie Arkin",semesterStart:2,semesterEnd:2,course:new Course(identity:"AMS310",title:"Probability",school:school1).save(failOnError:true),block:scheduleBlockArray[1]))
			studentSchedule2.addToCourses(new CourseSection(teacher:"Edwin Tjoe",semesterStart:2,semesterEnd:2,course:new Course(identity:"EST201",title:"Tech in Society",school:school1).save(failOnError:true),block:scheduleBlockArray[2]))
			studentSchedule2.addToCourses(new CourseSection(teacher:"Ira Smith",semesterStart:2,semesterEnd:2,course:new Course(identity:"MUS101",title:"Music I",school:school1).save(failOnError:true),block:scheduleBlockArray[3]))
			studentSchedule2.addToCourses(new CourseSection(teacher:"Mark Bennard",semesterStart:2,semesterEnd:2,course:new Course(identity:"ESE120",title:"Circuit Design II",school:school1).save(failOnError:true),block:scheduleBlockArray[4]))
			student3.addToAssignedSchedules(studentSchedule2)
			student3.save(failOnError:true)

			//test case S2 year:2014 semester:1
			def school3=new School(name:"2 Semester School",numSemesters:2,numDays:2, numPeriods:6,lunchPeriodStart:4,lunchPeriodEnd:4)
			def school3TestYear=new AcademicYearScheduleBlockSet(school:school3,year:2014)

			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'1'))
			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'2'))
			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'1'))
			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'2'))
			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'1'))
			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'2'))
			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'1,2'))
			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'1,2'))
			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'4',days:'1,2'))
			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1,2'))
			school3TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'6',days:'1,2'))
			school3.addToAcademicYearSchedules(school3TestYear)
			school3.save(failOnError:true)
			scheduleBlockArray=school3TestYear.scheduleBlocks.toArray().sort{
				[it.periods, it.days]
			}
			def ams301 = new Course(identity:"AMS301",title:"Graph Theory",school:school3).save(failOnError:true)
			def ams310 = new Course(identity:"AMS310",title:"Statistics",school:school3).save(failOnError:true)
			def ams450 = new Course(identity:"AMS450",title:"Game Theory",school:school3).save(failOnError:true)
			def ams220 = new Course(identity:"AMS220",title:"Linear Algebra",school:school3).save(failOnError:true)
			new CourseSection(teacher:"James Xu",semesterStart:2,semesterEnd:2,
			course:ams301, block:scheduleBlockArray[0]).save(failOnError:true)
			new CourseSection(teacher:"Alan Tucker",semesterStart:1,semesterEnd:2,
			course:ams301, block:scheduleBlockArray[7]).save(failOnError:true)
			new CourseSection(teacher:"Hongshik Ahn",semesterStart:1,semesterEnd:1,
			course:ams310, block:scheduleBlockArray[9]).save(failOnError:true)
			new CourseSection(teacher:"Hongshik Ahn",semesterStart:2,semesterEnd:2,
			course:ams310, block:scheduleBlockArray[10]).save(failOnError:true)
			new CourseSection(teacher:"Ann Smith",semesterStart:1,semesterEnd:2,
			course:ams450, block:scheduleBlockArray[1]).save(failOnError:true)
			new CourseSection(teacher:"John Smith",semesterStart:1,semesterEnd:2,
			course:ams450, block:scheduleBlockArray[6]).save(failOnError:true)
			new CourseSection(teacher:"Ronald Re",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"AMS230",title:"Probablity",school:school3).save(failOnError:true),
			block:scheduleBlockArray[2]).save(failOnError:true)
			new CourseSection(teacher:"Sean Thomas",semesterStart:2,semesterEnd:2,
			course:ams220, block:scheduleBlockArray[3]).save(failOnError:true)
			new CourseSection(teacher:"Sean Connery",semesterStart:1,semesterEnd:1,
			course:ams220, block:scheduleBlockArray[8]).save(failOnError:true)
			new CourseSection(teacher:"Bob Falcone",semesterStart:1,semesterEnd:2,
			course:new Course(identity:"AMS100",title:"Algebra",school:school3).save(failOnError:true),
			block:scheduleBlockArray[4]).save(failOnError:true)
			new CourseSection(teacher:"Dennis Chan",semesterStart:1,semesterEnd:2,
			course:new Course(identity:"AMS105",title:"Calculus",school:school3).save(failOnError:true),
			block:scheduleBlockArray[5]).save(failOnError:true)
			def studentSchool3=new Student(email:"Amsmdd@gmail.com",firstName:"Zack",lastName:"Titus",username:"studentS2",password:"1234",school:school3)
			studentSchool3.save(failOnError:true);
			UserSecRole.create studentSchool3,studentRole,true

			//test case s4 year:2015 semester:1
			def school4=new School(name:"4 Semester School",numSemesters:4,numDays:2, numPeriods:6,lunchPeriodStart:4,lunchPeriodEnd:4)
			def school4TestYear=new AcademicYearScheduleBlockSet(school:school4,year:2015)

			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'1'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'2'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'1'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'2'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'1'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'2'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'1,2'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'1,2'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'1,2'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'4',days:'1,2'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1,2'))
			school4TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'6',days:'1,2'))
			school4.addToAcademicYearSchedules(school4TestYear)
			school4.save(failOnError:true)
			scheduleBlockArray=school4TestYear.scheduleBlocks.toArray().sort{
				[it.periods, it.days]
			}
			def his101 = new Course(identity:"HIS101",title:"American History I",school:school4).save(failOnError:true)
			def his204 = new Course(identity:"HIS204",title:"Russian Culture",school:school4).save(failOnError:true)
			def his210 = new Course(identity:"HIS210",title:"European History",school:school4).save(failOnError:true)
			def his209 = new Course(identity:"HIS209",title:"Imperial Russia",school:school4).save(failOnError:true)
			def his264 = new Course(identity:"HIS264",title:"The Early Republic",school:school4).save(failOnError:true)
			new CourseSection(teacher:"Bettie White",semesterStart:1,semesterEnd:4,
			course:his101, block:scheduleBlockArray[0]).save(failOnError:true)
			new CourseSection(teacher:"Betty Crocker",semesterStart:1,semesterEnd:1,
			course:his101, block:scheduleBlockArray[4]).save(failOnError:true)
			new CourseSection(teacher:"Jack White",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"HIS102",title:"American History II",school:school4).save(failOnError:true),
			block:scheduleBlockArray[1]).save(failOnError:true)
			new CourseSection(teacher:"Sean Roley",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"HIS201",title:"Asian Culture and History",school:school4).save(failOnError:true),
			block:scheduleBlockArray[2]).save(failOnError:true)
			new CourseSection(teacher:"Lawrence Jao",semesterStart:1,semesterEnd:1,
			course:his210, block:scheduleBlockArray[3]).save(failOnError:true)
			new CourseSection(teacher:"John Locke",semesterStart:3,semesterEnd:3,
			course:his210, block:scheduleBlockArray[7]).save(failOnError:true)
			new CourseSection(teacher:"Tim Hyde",semesterStart:4,semesterEnd:4,
			course:his204, block:scheduleBlockArray[4]).save(failOnError:true)
			new CourseSection(teacher:"Tom Jekyll",semesterStart:1,semesterEnd:4,
			course:his204, block:scheduleBlockArray[9]).save(failOnError:true)
			new CourseSection(teacher:"Fyodor Dostoyevsky",semesterStart:1,semesterEnd:4,
			course:his209, block:scheduleBlockArray[5]).save(failOnError:true)
			new CourseSection(teacher:"Nikolai Gogol",semesterStart:1,semesterEnd:4,
			course:his209, block:scheduleBlockArray[1]).save(failOnError:true)
			new CourseSection(teacher:"John Newman",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"HIS203",title:"Ancient Rome",school:school4).save(failOnError:true),
			block:scheduleBlockArray[6]).save(failOnError:true)
			new CourseSection(teacher:"Benedict Arnold",semesterStart:1,semesterEnd:4,
			course:new Course(identity:"HIS215",title:"Long Island History",school:school4).save(failOnError:true),
			block:scheduleBlockArray[7]).save(failOnError:true)
			new CourseSection(teacher:"Ibn Battuta",semesterStart:2,semesterEnd:2,
			course:new Course(identity:"HIS227",title:"Islamic Civilization",school:school4).save(failOnError:true),
			block:scheduleBlockArray[8]).save(failOnError:true)
			new CourseSection(teacher:"Toni Morrison",semesterStart:1,semesterEnd:4,
			course:new Course(identity:"HIS236",title:"The Late Middle Ages",school:school4).save(failOnError:true),
			block:scheduleBlockArray[9]).save(failOnError:true)
			new CourseSection(teacher:"Thurgood Marshall",semesterStart:1,semesterEnd:1,
			course:his264, block:scheduleBlockArray[10]).save(failOnError:true)
			new CourseSection(teacher:"John Jay",semesterStart:1,semesterEnd:1,
			course:his264, block:scheduleBlockArray[8]).save(failOnError:true)

			def studentSchool4=new Student(email:"CFitz@gmail.com",firstName:"Corey",lastName:"Fitz",
			username:"studentS4",password:"1234",school:school4)
			studentSchool4.save(failOnError:true);
			UserSecRole.create studentSchool4,studentRole,true


			//test case D5
			def school5=new School(name:"D5 School",numSemesters:2,numDays:5, numPeriods:6,lunchPeriodStart:4,lunchPeriodEnd:4)
			def school5TestYear=new AcademicYearScheduleBlockSet(school:school5,year:2014)

			school5TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'1',days:'1,2')) //5
			school5TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'1,4')) //6
			school5TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'2,5')) //7
			school5TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'2,5')) //8
			school5TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'4',days:'3,4')) //9
			school5TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1')) //3
			school5TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'6',days:'4')) //4
			school5TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'2',days:'1,2,3,4,5')) //0
			school5TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'3',days:'1,2,3,4,5')) //1
			school5TestYear.addToScheduleBlocks(new ScheduleBlock(periods:'5',days:'1,2,3,4,5')) //2
			school5.addToAcademicYearSchedules(school5TestYear)
			school5.save(failOnError:true)
			scheduleBlockArray=school5TestYear.scheduleBlocks.toArray().sort{
				[it.periods, it.days]
			}
			new CourseSection(teacher:"James Re",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"AMS301",title:"Graph Theory",school:school5).save(failOnError:true),
			block:scheduleBlockArray[0]).save(failOnError:true)
			new CourseSection(teacher:"James Re",semesterStart:1,semesterEnd:1,
			course:Course.findWhere(identity:"AMS301",school:school5),
			block:scheduleBlockArray[3]).save(failOnError:true)
			new CourseSection(teacher:"Sam Robert",semesterStart:1,semesterEnd:1,
			course:Course.findWhere(identity:"AMS301",school:school5),
			block:scheduleBlockArray[6]).save(failOnError:true)
			new CourseSection(teacher:"Thomas Klien",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"JPN102",title:"Japanese History",school:school5).save(failOnError:true),
			block:scheduleBlockArray[2]).save(failOnError:true)
			new CourseSection(teacher:"Dave Thomas",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"SOC102",title:"Social Study",school:school5).save(failOnError:true),
			block:scheduleBlockArray[7]).save(failOnError:true)
			new CourseSection(teacher:"Allen Henry",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"PHI105",title:"Philosophy and Society",school:school5).save(failOnError:true),
			block:scheduleBlockArray[4]).save(failOnError:true)
			new CourseSection(teacher:"Tom Hyde",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"PHI102",title:"Politics",school:school5).save(failOnError:true),
			block:scheduleBlockArray[5]).save(failOnError:true)
			new CourseSection(teacher:"Anthony Hughes",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"MIL",title:"Military Tactics",school:school5).save(failOnError:true),
			block:scheduleBlockArray[6]).save(failOnError:true)
			new CourseSection(teacher:"Roger Goodell",semesterStart:1,semesterEnd:1,
			course:new Course(identity:"MAT107",title:"Algebra",school:school5).save(failOnError:true),
			block:scheduleBlockArray[8]).save(failOnError:true)
			new CourseSection(teacher:"Roger Goodell",semesterStart:1,semesterEnd:1,
			course:Course.findWhere(identity:"MAT107",school:school5),
			block:scheduleBlockArray[9]).save(failOnError:true)

			def studentschool5=new Student(email:"studentchool5@gmail.com",firstName:"David",lastName:"Gold",username:"studentD5",password:"1234",school:school5)
			studentschool5.save(failOnError:true);
			UserSecRole.create studentschool5,studentRole,true


			/*def studentCriteria=new DesiredScheduleCriteria(student:student)
			 studentCriteria.addToLunchDesiredDays(3)
			 studentCriteria.addToLunchDesiredDays(4)
			 studentCriteria.addToCourses(new DesiredScheduleCourse(course:c308,teacher:'Stoller'))
			 studentCriteria.addToCourses(new DesiredScheduleCourse(course:c373))
			 studentCriteria.save(failOnError:true)*/

			def pendingStudent1=new Student(enabled:false,email:"sgsdg@gmail.com",firstName:"Jack",lastName:"Bauer",school:school1,username:"jbauer",password:"1234",).save(failOnError:true);
			def pendingStudent2=new Student(enabled:false,email:"sttttg@gmail.com",firstName:"Tom",lastName:"Smith",school:school1,username:"tsmith",password:"1234",).save(failOnError:true);
			def pendingStudent3=new Student(enabled:false,email:"aaaaa@gmail.com",firstName:"Jerry",lastName:"Jones",school:school1,username:"jjones",password:"1234",).save(failOnError:true);

			new AccountRequest(student:pendingStudent1,dateTime:new Date()).save(failOnError:true)
			new AccountRequest(student:pendingStudent2,dateTime:new Date()).save(failOnError:true)
			new AccountRequest(student:pendingStudent3,dateTime:new Date()).save(failOnError:true)
		}
	}
	def destroy = {
	}
}
