class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/schedule/validateCourse/$year/$courseId"(controller:"schedule",
        	action:"validateCourse")
        "/"(view:"/index")
        "500"(view:'/error')
	}
}
