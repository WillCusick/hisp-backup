import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.ConcurrentSessionControlStrategy;
import org.springframework.security.web.session.ConcurrentSessionFilter;

// Place your Spring DSL code here
beans = {
	sessionRegistry(SessionRegistryImpl)
	concurrentSessionFilter(ConcurrentSessionFilter){
		sessionRegistry=sessionRegistry
		expiredUrl='/login/'
	}
	sessionAuthenticationStrategy(ConcurrentSessionControlStrategy, sessionRegistry) {
		maximumSessions = 1
	}
}
