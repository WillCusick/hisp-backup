<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="student">
<g:set var="entityName" value="Manage Friends" />
<title>Add Friends</title>
<script type="text/javascript">
$(function() {
	$("#searchFriendButton").click(function(){
		searchForFriend();
	});
	$("#nameTextBox").bind('keypress', function(e) {
		if(e.keyCode==13){
			searchForFriend();
		}
	});
});
function searchForFriend(){
	$.post("${createLink(action: 'searchFriends')}",
			{name:$("#nameTextBox").val()},
			function(data, status){
				$("#friendSearchResult").html(data);
	});	
}
</script>
</head>
<body>
	<h2>Add Friends</h2>
	<g:if test="${flash.message}">
		<div class="alert alert-success" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.error}">
		<div class="alert alert-danger" role="status">
			${flash.error}
		</div>
	</g:if>
	<div class="row" id="friendSearchBox">
		<div class="col-md-10">
			<g:textField id="nameTextBox" name="name" class="form-control"
				placeholder="Search name" />
		</div>
		<div class="col-md-2">
			<button id="searchFriendButton" class="btn btn-primary">
				<i class="fa fa-search fa-lg"></i> Search
			</button>
		</div>
	</div>
	<div class="row">
		<div id="friendSearchResult" class="col-md-12"></div>
	</div>
</body>
</html>
