
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="student">
<g:set var="entityName" value="Friend Requests" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<script type="text/javascript">
$(function() {
	$(".acceptRequestBtn").click(function(){
		var btn=$(this);
		var row=$(btn).closest(".row")
		$.ajax({
		    url: "${createLink(controller:'friends',action: 'acceptFriendRequest')}",
		    type: "POST",
		    data: {id:$(btn).attr('studentId')},
		    success: function(data, textStatus, xhr) {
		    	row.remove();
		    },
		    error: function (jqXHR, textStatus,errorThrown){
				bootbox.alert(errorThrown);
			}
		})
	});
	$(".rejectRequestBtn").click(function(){
		var btn=$(this);
		var row=$(btn).closest(".row")
		$.ajax({
		    url: "${createLink(controller:'friends',action: 'declineFriendRequest')}",
		    type: "POST",
		    data: {id:$(btn).attr('studentId')},
		    success: function(data, textStatus, xhr) {
		    	row.remove();
		    },
		    error: function (jqXHR, textStatus,errorThrown){
				bootbox.alert(errorThrown);
			}
		})
	});
});
</script>
</head>
<body>
	<h2>My Friend Requests</h2>
	<g:if test="${flash.message}">
		<div class="alert alert-success" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.error}">
		<div class="alert alert-danger" role="status">
			${flash.error}
		</div>
	</g:if>
	<g:each in="${requests}" var="r">
		<div class="row friendListBox">
			<div class="col-md-9">
				<div class="col-md-12 name">
					${r.firstName+" "+r.lastName}
				</div>
				<div class="col-md-12">
					${r.username}
				</div>
				<div class="col-md-12">
					${r.school.name}
				</div>
			</div>
			<div class="col-md-3 actionGroup">
				<button class="btn btn-success acceptRequestBtn" studentId="${r.id}">Accept</button>
				<button class="btn btn-danger rejectRequestBtn" studentId="${r.id}">Reject</button>
			</div>
		</div>
	</g:each>
</body>
</html>
