
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="student">
<g:set var="entityName"
	value="${message(code: 'school.label', default: 'My Friends')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<script type="text/javascript">
$(function(){
	$(".viewFriendAssignedBtn").click(function(){
		var btn=$(this)
		bootbox.dialog({
			title:"View Friend Schedule",
			message:"<div class='row'><div class='col-md-12'>" +
					"<div class='form-group col-md-9'>" +
					"<label for='yearField'>Year</label>" +
					"<input type='text' class='form-control' id='yearField'>" +
					"<label for='semesterField'>Semester</label>" +
					"<input type='text' class='form-control' id='semesterField'>" +
					"<p id='validationMessage' class='text-muted'></p>" +
					"</div></div></div>",
			buttons: {
				success: {
					label: "Submit",
					className: "btn btn-success",
					callback: function() {
						var year=$("#yearField").val()
						var semester=$("#semesterField").val()
						$.post("${createLink(controller: 'schedule', action: 'assignedSchedule')}",
								{year:year,semester:semester,username:$(btn).attr('username')},
								function(data, status){
									bootbox.alert(data);
						});
					}
				}
			}
		});
	})
	$(".viewFriendDesiredBtn").click(function(){
		var btn=$(this)
		$.post("${createLink(controller: 'schedule', action: 'desiredSchedule')}",
				{username:$(btn).attr('username')},
				function(data, status){
					bootbox.alert(data);
		});
	})
})
</script>
</head>
<body>
	<h2>My Friends</h2>
	<g:if test="${flash.message}">
		<div class="alert alert-success" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.error}">
		<div class="alert alert-danger" role="status">
			${flash.error}
		</div>
	</g:if>
	<g:each in="${friends}" var="f">
		<div class="row friendListBox">
			<div class="col-md-10">
				<div class="col-md-12 name">
					${f.firstName+" "+f.lastName}
				</div>
				<div class="col-md-12">
					${f.school.name}
				</div>
				<div class="col-md-12">
					<button class="viewFriendAssignedBtn btn btn-link btn-xs" username="${f.username}">View Assigned Schedule</button>
				</div>
				<div class="col-md-12">
					<button class="viewFriendDesiredBtn btn btn-link btn-xs" username="${f.username}">View Desired Schedule</button>
				</div>
				
			</div>
			<div class="col-md-2">
				<g:link controller="friends" action="unfriend" id="${f.id}"
					class="btn btn-danger action-btn"
					title="Unfriend ${f.firstName+" "+f.lastName}">
					<i class="fa fa-trash-o fa-lg"></i>
				</g:link>
			</div>
		</div>
	</g:each>
</body>
</html>
