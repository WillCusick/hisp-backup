<g:each in="${result}" var="r">
	<div class="row friendListBox">
		<div class="col-md-10">
			<p>
				Name: ${r.key.firstName}
				${r.key.lastName}
			</p>
			<p>
				Username: ${r.key.username}
			</p>
			<p>
				School: ${r.key.school.name}
			</p>
		</div>
		<div class="col-md-2">
			<g:if test="${r.value=='stranger'}">
			<button studentId="${r.key.id}"
				class="btn btn-primary sendRequestBtn">Add Friend</button>
			</g:if>
			<g:elseif test="${r.value=='friend'}">
			<button studentId="${r.key.id}"
				class="btn disabled">Friend</button>
			</g:elseif>
			<g:elseif test="${r.value=='recieved'}">
			<button studentId="${r.key.id}"
				class="btn btn-primary acceptRequestBtn">Accept Request</button>
			</g:elseif>
			<g:elseif test="${r.value=='sent'}">
			<button studentId="${r.key.id}"
				class="btn btn-success disabled">Request Sent</button>
			</g:elseif>
		</div>
	</div>
</g:each>
<script>
$(".sendRequestBtn").click(function(){
	var btn=$(this);
	$.ajax({
	    url: "${createLink(controller:'friends',action: 'sendFriendRequest')}",
	    type: "POST",
	    data: {id:$(this).attr('studentId')},
	    success: function(data, textStatus, xhr) {
		    btn.attr('class',"btn btn-success disabled");
		    btn.html("Request Sent");
	    },
	    error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(errorThrown);
		}
	})
});
$(".acceptRequestBtn").click(function(){
	var btn=$(this);
	$.ajax({
	    url: "${createLink(controller:'friends',action: 'acceptFriendRequest')}",
	    type: "POST",
	    data: {id:$(this).attr('studentId')},
	    success: function(data, textStatus, xhr) {
		    btn.attr('class',"btn disabled");
		    btn.html("Friend");
	    },
	    error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(errorThrown);
		}
	})
});
</script>