<html>

<head>
<title><g:message code='spring.security.ui.login.title' /></title>
<meta name='layout' content='main' />
<style type="text/css">
#loginForm {
	margin-left: auto;
	margin-right: auto;
	width: 50%;
}
</style>
</head>

<body>

	<p />

	<div>
		<div class="login-inner">
			<div class="row">
				<g:if test="${params?.login_error}">
					<br>
					<div class="alert alert-danger">
						<h3>Errors</h3>
						Invalid username and password
					</div>
				</g:if>
			</div>
			<form action='${postUrl}' method='POST' id="loginForm"
				name="loginForm" autocomplete='off'>
				<div class="sign-in">
					<div class="row">
						<h1>
							Sign in
						</h1>
					</div>
					<div class="row">
						<div class="col-md-4 formLabel">
							<label for="username"><g:message
									code='spring.security.ui.login.username' /></label>
						</div>
						<div class="col-md-8">
							<input name="j_username" id="username" size="20"
								class="form-control" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 formLabel">
							<label for="password"><g:message
									code='spring.security.ui.login.password' /></label>
						</div>
						<div class="col-md-8">
							<input type="password" name="j_password" id="password" size="20"
								class="form-control" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"></div>
						<div class="col-md-8">
							<g:submitButton name="loginButton" value="Login"
								class="btn btn-primary" />
							<g:link controller="register">Request an account</g:link>
						</div>
					</div>

				</div>
			</form>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$('#username').focus();
		});
	</script>

</body>
</html>
