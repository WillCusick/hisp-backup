<%@ page import="com.student.Student" %>



<div class="fieldcontain">
	<label for="username">
		<g:message code="student.username.label" default="Username" />
	</label>
	<input class="form-control" value="${student?.username}" type="text" disabled>

</div>


<div class="fieldcontain ${hasErrors(bean: student, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="student.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="email" required="" value="${student?.email}" class="form-control"/>

</div>

<div class="fieldcontain ${hasErrors(bean: student, field: 'password', 'error')}">
	<label for="password">
		<g:message code="student.password.label" default="Password" />
	</label>
	<g:textField name="password" class="form-control"/>
</div>

<div class="fieldcontain ${hasErrors(bean: student, field: 'password', 'error')}">
	<label for="passwordConfirm">
		<g:message code="student.password.label" default="Confirm Password" />
	</label>
	<g:textField name="passwordConfirm" class="form-control"/>
</div>

<div class="fieldcontain ${hasErrors(bean: student, field: 'firstName', 'error')} required">
	<label for="firstName">
		<g:message code="student.firstName.label" default="First Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="firstName" required="" value="${student?.firstName}" class="form-control"/>

</div>

<div class="fieldcontain ${hasErrors(bean: student, field: 'lastName', 'error')} required">
	<label for="lastName">
		<g:message code="student.lastName.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="lastName" required="" value="${student?.lastName}" class="form-control"/>

</div>

<div class="fieldcontain ${hasErrors(bean: student, field: 'school', 'error')} required">
	<label for="school">
		<g:message code="student.school.label" default="School" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="school" name="school.id" from="${com.administration.School.list()}" optionKey="id" required="" value="${student?.school?.id}" class="many-to-one form-control"/>

</div>

