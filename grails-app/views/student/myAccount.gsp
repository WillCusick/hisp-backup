<%@ page import="com.student.Student"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="student">
<g:set var="entityName"
	value="${message(code: 'student.label', default: 'Student')}" />
<title>My Account</title>
</head>
<body>
	<div id="edit-student" class="content" role="main">
		<h1>My Account</h1>
		<g:if test="${flash.message}">
			<div class="alert alert-success" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:if test="${flash.errors}">
			<div class="row">
				<br />
				<div class="alert alert-danger">
					<h3>Errors</h3>
					<ul>
						<g:each in="${flash.errors}" var="error">
							<li>
								${error}
							</li>
						</g:each>
					</ul>
				</div>
			</div>
		</g:if>
		<div class="row">
			<div class="col-md-5">
				<g:form url="[action:'updateAccount']" method="PUT">
					<fieldset class="form">
						<g:render template="form" />
					</fieldset>
					<fieldset class="buttons">
						<g:actionSubmit class="save" action="updateAccount"
							class="btn btn-primary"
							value="${message(code: 'default.button.update.label', default: 'Update')}" />
					</fieldset>
				</g:form>
			</div>
		</div>
	</div>
</body>
</html>
