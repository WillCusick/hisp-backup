
<%@ page import="com.student.Student" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="student">
		<g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}" />
		<title>Student Dashboard</title>
	</head>
	<body>
		<h2>Welcome, <g:currentName/></h2>
	</body>
</html>
