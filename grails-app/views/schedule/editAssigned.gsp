<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="student">
<g:set var="entityName"
	value="${message(code: 'school.label', default: 'My Assigned Schedule')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<script type="text/javascript">
var hisp = {};
function showCourseIdDialog() {
	bootbox.dialog({
		title:"Add a Course",
		message:"<div class='row'><div class='col-md-12'>" +
				"<div class='form-group col-md-9'>" +
				"<label for='courseIdField'>Course Idenitifer</label>" +
				"<input type='text' class='form-control' id='courseIdField'>" +
				"<p id='validationMessage' class='text-muted'></p>" +
				"</div></div></div>",
		buttons: {
			success: {
				label: "Continue",
				className: "btn btn-success",
				callback: function() {
					showCourseTitleDialog();
				}
			}
		}
	});
	$("#courseIdField").bind("input", function() {
		hisp.courseId = $("#courseIdField").val();
		var validationLink = "${createLink(controller:'schedule', action:
			'validateCourse')}";
		var year = $("#year").val();
		var semester = $("#semester").val();
		$.getJSON(validationLink+"/"+year+"/"+semester+"/"+hisp.courseId, validateCourse);
	});
}

function validateCourse(data) {
	if (hisp.courseId != $("#courseIdField").val())
		return; // outdated information
	if (data.seenThisYear == true) {
		$("#validationMessage").html("Course "+hisp.courseId+" was found in our database.<br>Press continue to confirm.");
		hisp.seen = "thisYear";

	} else if (data.seenPrevYears == true) {
		$("#validationMessage").html(
			"Course "+hisp.courseId+" was found for a previous year.<br>" + 
			"Press continue to confirm.");
		hisp.seen = "prevYear";
	} else {
		$("#validationMessage").html(
			"Course "+hisp.courseId+" was not found in our database.<br>" + 
			"Press continue to manually enter the course information.");
		hisp.seen = "never";
	}
	hisp.courseTitle = data.courseTitle;
}

function showCourseTitleDialog() {
	var message = "<div class='row'><div class='col-md-12'>" +
				"<div class='form-group col-md-9'>" +
				"<label for='courseTitleField'>Course Name</label>";
	switch(hisp.seen) {
		case "thisYear":
			message += "<input type='text' class='form-control' id='courseTitleField' value='"+hisp.courseTitle+"' disabled=true>" +
				"<p id='validationMessage' class='text-muted'></p>" +
				"</div></div></div>";
			break;
		case "prevYear":
			message += "<input type='text' class='form-control' id='courseTitleField' value='"+hisp.courseTitle+"'>" +
				"<p id='validationMessage' class='text-muted'>Please edit the title if necessary</p>" +
				"</div><</div></div>";
			break;
		case "never":
			message += "<input type='text' class='form-control' id='courseTitleField'>" +
				"<p id='validationMessage' class='text-muted'>Please add a title if necessary</p>" +
				"</div></div></div>";
			break;
		default:
			return;
	}
	bootbox.dialog({
		title:"Add a Course",
		message:message,
		buttons: {
			success: {
				label: "Continue",
				className: "btn btn-success",
				callback: function() {
					updateCourseTitle();
					showScheduleBlockDialog();
				}
			}
		}
	});
	$("#courseTitleField").bind("input", function() {
		hisp.courseTitle = $("#courseTitleField").val();
	});
}

function updateCourseTitle() {
	$.ajax({
		url: "${createLink(controller:'schedule',action: 'updateCourseTitle')}",
		data: {year: $("#year").val(),
			semester: $("#semester").val(),
			courseId: hisp.courseId,
			courseTitle: hisp.courseTitle},
		type: "POST",
		error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(jqXHR.responseText);
		}
	});
}

function showScheduleBlockDialog() {
	bootbox.dialog({
		title:"Add a Course",
		message:"<div class='row'><div class='col-md-12'>" +
				"<div class='form-group col-md-9'>" +
				$("#hidden").html() +
				"<p id='validationMessage' class='text-muted'></p>" +
				"</div></div></div>",
		buttons: {
			success: {
				label: "Continue",
				className: "btn btn-success",
				callback: function() {
					validateScheduleBlock();
				}
			}
		}
	});
}

function validateScheduleBlock() {
	hisp.periods = $(".bootbox-body #periodField").val().join(',');
	hisp.days = $(".bootbox-body #dayField").val().join(',');
	$.ajax({
		url: "${createLink(controller:'schedule',action: 'validateScheduleBlock')}",
		data: {year: $("#year").val(),
			semester: $("#semester").val(),
			courseId: hisp.courseId,
			periods: hisp.periods,
			days: hisp.days},
		async: true,
		type: "POST",
		success: function(data, textStatus, xhr) {
			if (!data.valid)
				bootbox.alert("Sorry, that is not a legal schedule block");
			else {
				hisp.instructor = data.instructor;
				hisp.sectionSeen = data.sectionSeen;
				showInstructorDialog();
			}
		},
		error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(jqXHR.responseText);
		}
	});
}

function showInstructorDialog() {
	var message = "<div class='row'><div class='col-md-12'>" +
		"<div class='form-group col-md-9'>" +
		"<label for='instructorField'>Instructor Name</label>";
	if(hisp.sectionSeen) {
		message += "<input type='text' class='form-control' id='instructorField' value='"+hisp.instructor+"' disabled=true>";
	} else {
		message += "<input type='text' class='form-control' id='instructorField'>";
	}
	message += "<p id='validationMessage' class='text-muted'></p>" +
		"</div></div></div>";
	bootbox.dialog({
		title:"Add a Course",
		message:message,
		buttons: {
			success: {
				label: "Finish",
				className: "btn btn-success",
				callback: function() {
					hisp.instructor = $("#instructorField").val();
					updateInstructor();
					addCourse();
				}
			}
		}
	});
}

function updateInstructor() {
	$.ajax({
		url: "${createLink(controller:'schedule',action: 'updateInstructor')}",
		data: {year: $("#year").val(),
			semester: $("#semester").val(),
			courseId: hisp.courseId,
			periods: hisp.periods,
			days: hisp.days,
			instructor: hisp.instructor},
		type: "POST",
		error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(jqXHR.responseText);
		}
	});
}

function addCourse() {
	$.ajax({
		url: "${createLink(controller:'schedule',action: 'addCourseToAssigned')}",
		data: {year: $("#year").val(),
			semester: $("#semester").val(),
			courseId: hisp.courseId,
			periods: hisp.periods,
			days: hisp.days},
		async: true,
		type: "POST",
		success: function(data, textStatus, xhr) {
			bootbox.alert("Course added successfully");
			$("#findScheduleButton").click(); // update schedule
		},
		error: function (jqXHR, textStatus,errorThrown){
			bootbox.alert(jqXHR.responseText);
		}
	});
}

$(function(){
	$("#findScheduleButton").click(function(){
		$.ajax({
		    url: "${createLink(controller:'schedule',action: 'getAssignedScheduleEditForm')}",
		    type: "POST",
		    data: {year : $("#year").val(),
				semester : $("#semester").val()},
		    success: function(data, textStatus, xhr) {
				$("#addCourseBtn").show()
				$("#scheduleContainer").html(data);
		    },
		    error: function (jqXHR, textStatus,errorThrown){
				bootbox.alert(jqXHR.responseText);
			}
		})		
	})

	$("#addCourseBtn").click(function() {
		if (!$.isEmptyObject(hisp))
			hisp = {};
		$("#hidden #periodField").prop('multiple', true);
		$("#hidden #dayField").prop('multiple', true);
		showCourseIdDialog();
	});
})
</script>
</head>
<body>
	<h2 class="pageHeader">Edit Assigned Schedule</h2>
	<g:if test="${flash.message}">
		<div class="alert alert-success" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.error}">
		<div class="alert alert-danger" role="status">
			${flash.error}
		</div>
	</g:if>
	<div id="scheduleSelector" class="row">
		<div class="col-md-12">
			<label>Year</label>
			<g:select id="year" name="year" from="${minYear..maxYear}"
				value="${currentYear}" />
			<label>Semester</label>
			<g:select id="semester" name="semester" from="${1..maxSemester}" />
			<button id="findScheduleButton" name="findScheduleButton"
				class="btn btn-primary">Get Schedule</button>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12">
			<button id="addCourseBtn" class="btn btn-success pull-right" style="display: none;">Add Course</button>
		</div>
	</div>
	<div id="scheduleContainer" style="margin-top: 20px"></div>
	<span id="hidden" style="display:none">
	<label for='periodField'>Period(s)</label>
	<g:select name="periodField" class="form-control" id="periodField" from="${1..periods}"/>
	<label for='dayField'>Day(s)</label>
	<g:select name="dayField" class="form-control" id="dayField" from="${1..days}"/>
</span>
</body>
</html>