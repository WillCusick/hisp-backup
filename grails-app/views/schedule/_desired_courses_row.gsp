<div class="row course">
	<div class="col-md-5">
		<input name="desiredCourseIdentity" class="desiredCourseIdentityField" type="hidden" value="${desiredScheduleCourseInstance?.course?.identity }">
		${desiredScheduleCourseInstance?.course?.identity} - ${desiredScheduleCourseInstance?.course?.title}
	</div>
	<div class="col-md-5">
		<g:textField name="desiredCourseTeacher" class="form-control" placeholder="Teacher" value="${desiredScheduleCourseInstance?.teacher }" />
	</div>
	<div class="col-md-2">
		<a href="javascript:void(0);" class="btn btn-danger"
			onclick="removeCourse(this);"><i class="fa fa-minus"></i></a>
	</div>
</div>