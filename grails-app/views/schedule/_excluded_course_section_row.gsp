<tr class="excludeCourseSection">
	<td><input type="hidden" value="${section.id}"
		name="excludedCourseIds" class="excludedCourseId"> ${section.course.identity}
	</td>
	<td>
		${section.teacher}
	</td>
	<td>
		${section.block.periods}
	</td>
	<td>
		${section.block.days}
	</td>
	<td>
		<% if(section.semesterStart == section.semesterEnd) {%>
		<%=section.semesterStart%>
		<%} else {%>
		<%="${section.semesterStart} - ${section.semesterEnd}"%>
		<% } %>
	</td>
	<td><a href="javascript:void(0);" class="btn btn-danger"
		onclick="removeExcludeCourseSection(this);"><i class="fa fa-minus"></i></a>
	</td>
</tr>