<html>
<head>
<meta name="layout" content="student">
<g:set var="entityName" value="Desired Schedule" />
<title><g:message code="default.list.label" args="[entityName]" /></title>

<script type="text/javascript">
$(function(){
    var hispUrls = {
            exportScheduleLink: "${createLink(controller:'schedule',action: 'exportDesired')}",
            exportScheduleWithFriendsLink: "${createLink(controller:'schedule',action: 'exportDesiredWithFriends')}",
            getScheduleWithFriendsLink: "${createLink(controller:'schedule',action:'desiredWithFriends')}",
            getScheduleLink: "${createLink(controller:'schedule',action:'desired')}"
    };
    
    $('#showHideFriends').change(function() {
        if(this.checked){
            $(".friendsHeader").show();
            $(".friendsValue").show();
            $("#submitExport").prop('value', 'Export Desired Schedule With Friends');
        } else {
            $(".friendsHeader").hide();
            $(".friendsValue").hide();
            $("#submitExport").prop('value', 'Export Desired Schedule');
        }
    });
    
    var hisp = hispUrls;
})    
    </script>
</head>
<body>
	<h2>Desired Schedule</h2>
	<g:if test="${flash.message}">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success" role="status">
					${flash.message}
				</div>
			</div>
		</div>
	</g:if>
	<g:if test="${flash.changes}">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger" role="status">
					<h4>Could not generate schedule</h4>
					<g:each in="${flash.changes}" var="c">
						<p style="font-weight: bold;">
							Changes for schedule:
							${c.key}
						</p>
						<ul>
							<g:each in="${c.value}" var="change">
								<li>
									${change}
								</li>
							</g:each>
						</ul>
					</g:each>
				</div>
			</div>
		</div>
	</g:if>
	<g:if test="${flash.error}">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger" role="status">
					${flash.error}
				</div>
			</div>
		</div>
	</g:if>
	<div class="row">
		<div class="col-md-12">
			<g:link controller="schedule" action="createDesiredSchedule"
				class="btn btn-primary generateScheduleBtn">Generate Desired Schedule</g:link>
			<g:if test="${schedule}">
				<g:form controller="schedule" action="exportDesired" style="display:inline">
					<g:submitButton name="submitExport" class="btn btn-primary generateScheduleBtn" value="Export Desired Schedule"/>
					<label for="showHideFriends" class="checkbox-inline pull-right">
						<input type="checkbox" id="showHideFriends" name="showHideFriends">Show Friends Schedules?
					</label>
				</g:form>
			</g:if>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<g:if test="${schedule}">
				<table class="table">
					<tr>
						<th>Period</th>
						<th>Days</th>
						<th>Course</th>
						<th>Teacher</th>
						<th class="friendsHeader" hidden>Friends</th>

					</tr>
					<g:each in="${courses}" var="c">
						<tr>
							<td>
								${c.block.periods}
							</td>
							<td>
								${c.block.days}
							</td>
							<td>
								${c.course.identity}
							</td>
							<td>
								${c.teacher}
							</td>
							<td class="friendsValue" hidden>
								${friendsInCourses[c].join(', ')}
							</td>
						</tr>

					</g:each>
				</table>
			</g:if>
			<g:else>

			</g:else>
		</div>
	</div>
	<asset:javascript src="scheduleAjax.js" />
</body>
</html>
