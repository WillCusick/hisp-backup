<div id="schedule">
	<table class="table">
		<tr>
			<th>Period</th>
			<th>Days</th>
			<th>Course</th>
			<th>Teacher</th>
			<g:if test="${showFriends}"><th>Friends</th></g:if>
			<th>Action</th>
		</tr>
		<g:each in="${courses}" var="c">
			<tr id="${c.id}">
				<td>
					${c.block.periods}
				</td>
				<td>
					${c.block.days}
				</td>
				<td>
					${c.course.identity} - ${c.course.title}
				</td>
				<td>
					${c.teacher}
				</td>
				<g:if test="${showFriends}">
				<td>
					${friends[c]?.join(', ')}
				</td>
				</g:if>
				<td><div class="actionGroup">
						<button class="removeCourseBtn confirmButton btn btn-danger" courseSectionId="${c.id}">
							<i class="fa fa-trash-o fa-lg"></i>
						</button>
					</div></td>
			</tr>
		</g:each>
	</table>
	<script type="text/javascript">
	$(".removeCourseBtn").click(function(){
		var btn=$(this);
		bootbox.confirm("Delete course?",function(result){
			if (!result)
				return;
			$.ajax({
			    url: "${createLink(controller:'schedule',action: 'removeFromSchedule')}",
			    type: "POST",
			    data: {courseSectionId:$(btn).attr('courseSectionId')},
			    success: function(data, textStatus, xhr) {
				    $(btn).closest("tr").remove();
				    bootbox.alert("Course removed")
			    },
			    error: function (jqXHR, textStatus,errorThrown){
					bootbox.alert(jqXHR.responseText);
				}
			})
		})
	});
	</script>
</div>
