<div class="fieldcontain required">
	<label for="year"> Year <span class="required-indicator">*</span>
	</label>
	<g:textField name="year"
		value="${desiredScheduleCriteriaInstance?.year}" required=""
		class="form-control yearTextbox"/>
</div>
<div class="fieldcontain required">
	<label for="semester"> Semester <span
		class="required-indicator">*</span>
	</label>
	<g:textField name="semester"
		value="${desiredScheduleCriteriaInstance?.semester}" required=""
		class="form-control semesterTextbox"/>
</div>

<div class="fieldcontain">
	<label for="lunchDesiredDays"> Lunch Days
	</label>
	<g:textField name="lunchDesiredDays"
		value="${desiredScheduleCriteriaInstance?.lunchDesiredDays}" class="form-control lunchDaysTextbox" />
<script type="text/javascript">
	$(function() {
		$(".yearTextbox, .semesterTextbox").change(function(){
			var e=$(this)
			var excludedSections=$(".excludeCourseSection").length
			if(excludedSections>0||$("#excludeCourseSectionSelect").html().trim().length!=0){
				bootbox.confirm("Changing year or semester will clear excluded course sections. Continue?", function(result) {
					if(result){
					  	$(".excludeCourseSection").remove()
					  	$("#excludeCourseIdSearch").val("")
					  	$("#excludeCourseSectionSelect").html("")
					}
					 else
						$(e).val($(e).data('oldVal'))
				}); 
			}
		})
		$(".yearTextbox, .semesterTextbox").focus(function(){
			$(this).data('oldVal',$(this).val())
		})
	})
</script>
</div>
<hr>
<div class="fieldcontain">
	<label for="courses">Desired Courses </label>

	<div class="row">
		<div class="col-md-10">
			<g:textField name="addCourseSearchBox" id="addCourseSearchBox"
				class="form-control" placeholder="Course ID" />
		</div>
		<div class="col-md-2">
			<a href="javascript:void(0);" id="addCourseBtn" class="btn btn-success pull-right"><i
				class="fa fa-plus fa-lg"></i></a>
		</div>
	</div>
	<label for="courseFormContainer">Courses </label>
	<div id="courseFormContainer">
		<g:if test="${!desiredScheduleCriteriaInstance?.courses?.isEmpty()}">
			<g:each in="${desiredScheduleCriteriaInstance?.courses}" var="c">
				<g:render template="/schedule/desired_courses_row"
					model="[desiredScheduleCourseInstance:c]" />
			</g:each>
		</g:if>

	</div>
	<script type="text/javascript">
		$(function() {			
			$(window).keydown(function(event){
			    if(event.keyCode == 13) {
			      event.preventDefault();
			      return false;
			    }
			  });
			$("#addCourseBtn").click(function() {
				var query=$("#addCourseSearchBox").val().trim()
				var courseIds=$(".desiredCourseIdentityField")
				var exists=false
				for(i=0;i<courseIds.length;i++)
				{
					if($(courseIds[i]).val().toUpperCase()===query.toUpperCase()){
						exists=true
						break
					}
				}
				if(!exists){
					$.ajax({
					    url: "${createLink(controller:'schedule',action: 'addToDesiredCourses')}",
					    type: "GET",
					    data: {identity:query},
					    success: function(data, textStatus, xhr) {					    	
							$("#courseFormContainer").append(data)
							$("#addCourseSearchBox").val("")
					    },
					    error: function (jqXHR, textStatus,errorThrown){
							bootbox.alert(jqXHR.responseText);
						}
					})
				}
			});
		});
		function removeCourse(e) {
			$(e).closest(".course").remove();
		}
	</script>
</div>

<hr>
<div class="fieldcontain">
	<label for="excludedSections"> Excluded Course Sections </label>
	<div id="excludedSectionsFormContainer">
		<div>
			<div class="row">
				<div class="col-md-3">
					<g:textField name="excludeCourseIdSearch"
						id="excludeCourseIdSearch"
						class="form-control excludeCourseIdTextbox"
						placeholder="Course ID" />
				</div>
				<div class="col-md-7">
					<select id="excludeCourseSectionSelect" class="form-control"
						class="excludeCourseSectionSelect">
					</select>
				</div>
				<div class="col-md-2">
					<a href="javascript:void(0);" id="addExcludeCourseSectionBtn"
						class="pull-right btn btn-success"><i class="fa fa-plus fa-lg"></i></a>
				</div>
			</div>
		</div>
		<table class="table" id="excludedSectionsTable">
			<tr>
				<th>ID</th>
				<th>Teacher</th>
				<th>Period</th>
				<th>Days</th>
				<th>Semesters</th>
				<th></th>
			</tr>
			<g:if
				test="${!desiredScheduleCriteriaInstance?.excludedSections?.isEmpty()}">
				<g:each in="${desiredScheduleCriteriaInstance?.excludedSections}"
					var="c">
					<g:render template="/schedule/excluded_course_section_row"
						model="[section:c]" />
				</g:each>
			</g:if>
		</table>
	</div>
	<script type="text/javascript">
		$(function() {
			$("#excludeCourseIdSearch").bind("input",function(){
				var query=$(this).val()
				var year=$(".yearTextbox").val()
				var semester=$(".semesterTextbox").val()
				if(year.length>0&&semester.length>0){
					$.ajax({
					    url: "${createLink(controller:'schedule',action: 'searchByCourseIdentifier')}",
					    type: "GET",
					    data: {identity:query,year:year,semester:semester},
					    success: function(data, textStatus, xhr) {
						    $("#excludeCourseSectionSelect").html(data)
					    },
					    error: function (jqXHR, textStatus,errorThrown){
							bootbox.alert(jqXHR.responseText);
						}
					})
				}
			})
			$("#addExcludeCourseSectionBtn")
					.click(
							function() {
								var newExcludeId = $("#excludeCourseSectionSelect").val()
								if (newExcludeId!=null){
									var excludedIds=$(".excludedCourseId")
									var exists=false
									for(i=0;i<excludedIds.length;i++)
									{
										if($(excludedIds[i]).val()==newExcludeId){
											exists=true
											break
										}
									}
									if(!exists){
										$.ajax({
										    url: "${createLink(controller:'schedule',action: 'addToExcludedCourseSections')}",
													type : "GET",
													data : {
														id : newExcludeId
													},
													success : function(data,
															textStatus, xhr) {
														$('#excludedSectionsTable').append(data)
													},
													error : function(jqXHR,
															textStatus,
															errorThrown) {
														bootbox.alert(jqXHR.responseText);
													}
												})
									}
								}
							});
		});
		function removeExcludeCourseSection(e) {
			$(e).closest(".excludeCourseSection").remove();
		}
	</script>
</div>
<hr>

