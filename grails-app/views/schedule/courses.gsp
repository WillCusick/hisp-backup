<%@ page import="com.schedule.Course"%>
<html>
<head>
<meta name="layout" content="student">
<g:set var="entityName"
	value="${message(code: 'course.label', default: 'Course')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div id="list-availableCourses" class="content scaffold-list"
		role="main">
		<h1>Available Courses</h1>
		<g:if test="${flash.message}">
			<div class="alert alert-success" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:if test="${flash.error}">
			<div class="alert alert-danger" role="status">
				${flash.error}
			</div>
		</g:if>
		<div class="row">
			<div class="col-md-12">
				<g:form action="courses">
		Year: <g:select id="year" name="year" from="${minYear..maxYear}"
						value="${currentYear}" />
					<g:submitButton name="submit" value="View Year" class="btn btn-primary" />
				</g:form>
			</div>
		</div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Course Identifier</th>
					<th>Title</th>
					<th>Teacher</th>
					<th>Year</th>
					<th>Semesters</th>
					<th>Periods</th>
					<th>Days</th>
					<th>Students in Course</th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${courseSectionsInstanceList}"
					var="courseSectionInstance">
					<tr>
						<td>
							${courseSectionInstance.course.identity}
						</td>
						<td>
							${courseSectionInstance.course.title}
						</td>
						<td>
							${courseSectionInstance.teacher}
						</td>
						<td>
							${courseSectionInstance.block.schoolAcademicYear.year}
						</td>
						<td>
							<% if(courseSectionInstance.semesterStart == courseSectionInstance.semesterEnd) {%>
							<%=courseSectionInstance.semesterStart%>
							<%} else {%>
							<%="${courseSectionInstance.semesterStart} - ${courseSectionInstance.semesterEnd}"%>
							<% } %>
						</td>
						<td>
							${courseSectionInstance.block.periods}
						</td>
						<td>
							${courseSectionInstance.block.days}
						</td>
						<td>
							${studentsCount[courseSectionInstance]}
						
					</tr>
				</g:each>
			</tbody>
		</table>
	</div>
</body>
</html>
