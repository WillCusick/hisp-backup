<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="student">
<g:set var="entityName"
	value="${message(code: 'school.label', default: 'My Assigned Schedule')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<script type="text/javascript">
var hispUrls = {
	validationLink: "${createLink(controller:'schedule', action:
			'validateCourse')}",
	updateCourseTitleLink: "${createLink(controller:'schedule',action: 'updateCourseTitle')}",
	validateScheduleBlockLink: "${createLink(controller:'schedule',action: 'validateScheduleBlock')}",
	updateTeacherLink: "${createLink(controller:'schedule',action: 'updateTeacher')}",
	addCourseLink: "${createLink(controller:'schedule',action: 'addCourseToAssigned')}",
	getScheduleLink: "${createLink(controller:'schedule',action: 'getAssignedScheduleEditForm')}"
};
var hisp = hispUrls;

$(function(){
	$("#findScheduleButton").click(getSchedule);
	$("#showHideFriends").click(getSchedule);

	$("#addCourseBtn").click(function() {
		if (!$.isEmptyObject(hisp))
			hisp = hispUrls;
		$("#hidden #periodField").prop('multiple', true);
		$("#hidden #dayField").prop('multiple', true);
		showCourseIdDialog();
	});

	%{-- auto-load the schedule if schedule exists --}%
	<g:if test="${scheduleExists}">
	getSchedule();
	</g:if>
});
</script>
</head>
<body>
<h2>My Assigned Schedule</h2>
<g:if test="${flash.message}">
	<div class="alert alert-success" role="status">
		${flash.message}
	</div>
</g:if>
<g:if test="${flash.error}">
	<div class="alert alert-danger" role="status">
		${flash.error}
	</div>
</g:if>
<div id="scheduleSelector" class="row">
	<div class="col-md-12">
		<label>Year</label>
		<g:if test="${scheduleExists}">
			<g:select id="year" name="year" from="${minYear..maxYear}"
			value="${year}" />
		</g:if>
		<g:else>
			<g:select id="year" name="year" from="${minYear..maxYear}"
			value="${currentYear}" />
		</g:else>
		<label>Semester</label>
		<g:if test="${scheduleExists}">
			<g:select id="semester" name="semester" from="${1..maxSemester}"
			value="${semester}" />
		</g:if>
		<g:else>
			<g:select id="semester" name="semester" from="${1..maxSemester}" />
		</g:else>
		<button id="findScheduleButton" name="findScheduleButton" class="btn btn-primary">Get Schedule</button>
		<br>
		<label for="showHideFriends">Show Friends Schedules?</label>
		<input type="checkbox" id="showHideFriends">
	</div>
</div>	
<div class="row">
	<div class="col-md-12">
		<button id="addCourseBtn" class="btn btn-success pull-right" style="display: none;">Add Course</button>
	</div>
</div>
<div id="scheduleContainer" style="margin-top: 20px"></div>
<span id="hidden" style="display:none">
	<div class="form-group">
		<label for='periodField'>Period(s)</label>
		<g:select name="periodField" class="form-control" id="periodField" from="${1..periods}"/>
	</div>
	<div class="form-group">
		<label for='dayField'>Day(s)</label>
		<g:select name="dayField" class="form-control" id="dayField" from="${1..days}"/>
	</div>
	<div class="form-group">
		<label for='semStartField'>Semester Start</label>
		<input type="number" class="form-control" name="semStartField" id="semStartField" min="1" value="${semester}">
	</div>
	<div class="form-group">
		<label for='semEndField'>Semester End</label>
		<input type="number" class="form-control" id="semEndField" name="semEndField" min="1" value="${semester}">
	</div>
</span>
<asset:javascript src="scheduleAjax.js"/>
</body>
</html>