<div id="schedule">
	<table class="table">
		<tr>
			<th>Period</th>
			<th>Days</th>
			<th>Course</th>
			<th>Teacher</th>
		</tr>
		<g:each in="${sections}" var="c">
			<tr>
				<td>
					${c.block.periods}
				</td>
				<td>
					${c.block.days}
				</td>
				<td>
					${c.course.identity} - ${c.course.title}
				</td>
				<td>
					${c.teacher}
				</td>
			</tr>
		</g:each>
	</table>
</div>
