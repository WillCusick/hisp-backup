<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><g:layoutTitle default="Grails" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}"
	type="image/x-icon">
<asset:stylesheet src="application.css" />
<asset:stylesheet src="font-awesome.min.css" />
<asset:stylesheet src="bootstrap.min.css" />
<asset:stylesheet src="main.css" />
<asset:javascript src="application.js" />
<asset:javascript src="bootbox.min.js" />
<g:layoutHead />
</head>
<body>
	<div id="pageContainer" class="container">
		<header>
			<div class="row">
				<div class="col-md-10">
					<a href="${createLink(uri: '/')}">
						<g:img file="hisplogo.png"/>
					</a>
				</div>
				<div class="col-md-2">
					<g:link controller="login" class="btn btn-primary">Login</g:link>
					<g:link controller="register" class="btn btn-primary">Sign Up</g:link>
				</div>
			</div>
		</header>
		<div id="contentBody">
			<g:layoutBody />
		</div>
		<div id="footer">
			<div id="copyNotice">&copy; 2015 HISP</div>
		</div>
		<div id="spinner" class="spinner" style="display: none;">
			<g:message code="spinner.alt" default="Loading&hellip;" />
		</div>
	</div>
</body>
</html>
