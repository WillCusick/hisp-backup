<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><g:layoutTitle default="Grails" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}"
	type="image/x-icon">
<asset:stylesheet src="application.css" />
<asset:stylesheet src="font-awesome.min.css" />
<asset:stylesheet src="bootstrap.min.css" />
<asset:stylesheet src="main.css" />
<asset:stylesheet src="admin.css" />
<asset:javascript src="application.js" />
<asset:javascript src="bootbox.min.js" />
<g:layoutHead />
<script type="text/javascript">
	$(function() {
		$("#sidebar").css('height', $("#pageContainer").height() + 50);
	})
</script>
</head>
<body>
	<div id="pageContainer" class="container">
		<div class="row">
			<div class="col-md-2 nopadding">
				<div id="sidebar">
					<ul>
						<li><div id="sidebarHeader">
								<g:img file="hispadminlogo.png" />
							</div></li>
						<li><g:link controller="school">Schools</g:link></li>
						<li><g:link controller="administrator" action="requests">Requests</g:link></li>
						<li><g:link controller="administrator"
								action="studentAccounts">Student Accounts</g:link></li>
					</ul>
					<ul id="bottomSidebar">
						<li><g:form name="logoutForm"
								url="[controller:'logout', action:'']">
								<button type="submit" class="btn btn-link">Logout</button>
							</g:form></li>
					</ul>
				</div>
			</div>
			<div id="contentBody" class="col-md-10">
				<g:layoutBody />
			</div>
		</div>
	</div>
</body>
</html>
