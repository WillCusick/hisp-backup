<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><g:layoutTitle default="Grails" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}"
	type="image/x-icon">
<asset:stylesheet src="application.css" />
<asset:stylesheet src="font-awesome.min.css" />
<asset:stylesheet src="bootstrap.min.css" />
<asset:stylesheet src="main.css" />
<asset:stylesheet src="student.css" />
<asset:javascript src="application.js" />
<asset:javascript src="bootbox.min.js" />
<g:layoutHead />
</head>
<body>
	<div id="pageContainer" class="container">
		<div id="logoNav">
			<div class="row">
				<div class="col-md-10">
					<g:img file="hisplogo.png" />
				</div>
				<div class="col-md-2">
					<g:form name="logout" method="POST"
						url="[controller:'logout', action:'']">
						<button type="submit" class="btn btn-danger">Logout</button>
					</g:form>
				</div>
			</div>
		</div>
		<div id="topNavContainer">
			<div id="topnav" class="row">
				<div class="col-md-4">
					<g:link controller="student">Home</g:link>
				</div>
				<div class="col-md-4"
					style="border-left: 1px solid #fff; border-right: 1px solid #fff;">
					<g:link controller="friends" action="myFriends">Friends</g:link>
				</div>
				<div class="col-md-4">
					<g:link controller="schedule" action="assigned">Courses</g:link>
				</div>
			</div>
		</div>
		<div id="contentBody">
			<div class="row">
				<div id="sidebarnav" class="col-md-2">
					<ul id="sidebarMenuList">
						<g:studentSiderbar></g:studentSiderbar>
					</ul>
				</div>
				<div id="content" class="col-md-10">
					<g:layoutBody />
				</div>
			</div>
		</div>
		<div id="footer">
					<div id="copyNotice">&copy; 2015 HISP</div>
				</div>

	</div>
</body>
</html>
