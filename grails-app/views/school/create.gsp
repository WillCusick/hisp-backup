<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="admin">
<g:set var="entityName"
	value="${message(code: 'school.label', default: 'School')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
</head>
<body>
	<div class="row">
		<div class="col-md-12">
			<div id="create-school" class="content scaffold-create" role="main">
				<h1>
					<g:message code="default.create.label" args="[entityName]" />
				</h1>
				<g:if test="${flash.message}">
					<div class="alert alert-success" role="status">
						${flash.message}
					</div>
				</g:if>
				<div class="row">
					<div class="col-md-7">
						<g:form url="[action:'save']">
							<fieldset class="form">
								<g:render template="form" />
							</fieldset>
							<fieldset class="buttons">
								<g:submitButton name="create" class="btn btn-primary"
									value="${message(code: 'default.button.create.label', default: 'Create')}" />
							</fieldset>
						</g:form>
					</div>
					<div class="col-md-7">
						<g:if test="${flash.errors||flash.error}">
							<div class="row">
								<br />
								<div class="alert alert-danger">
									<h3>Errors</h3>
									<g:if test="${flash.error }">
										${flash.error}
									</g:if>
									<g:else>
										<ul>
											<g:each in="${flash.errors}" var="error">
												<li>
													${error}
												</li>
											</g:each>
										</ul>
									</g:else>
								</div>
							</div>
						</g:if>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
