<%@ page import="com.administration.School"%>

<div
	class="fieldcontain ${hasErrors(bean: schoolInstance, field: 'name', 'error')} required">
	<label for="numSemesters"> <g:message code="school.name.label"
			default="Name" /> <span class="required-indicator">*</span>
	</label>
	<g:textField name="name" value="${schoolInstance.name}" required=""
		class="form-control" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: schoolInstance, field: 'numSemesters', 'error')} required">
	<label for="numSemesters"> <g:message
			code="school.numSemesters.label" default="Semesters" /> <span
		class="required-indicator">*</span>
	</label>
	<g:textField name="numSemesters" value="${schoolInstance.numSemesters}"
		required="" class="form-control" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: schoolInstance, field: 'numDays', 'error')} required">
	<label for="numDays"> <g:message code="school.numDays.label"
			default="Days" /> <span class="required-indicator">*</span>
	</label>
	<g:textField name="numDays" value="${schoolInstance.numDays}"
		required="" class="form-control" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: schoolInstance, field: 'numPeriods', 'error')} required">
	<label for="numPeriods"> <g:message
			code="school.numPeriods.label" default="Periods" /> <span
		class="required-indicator">*</span>
	</label>
	<g:textField name="numPeriods" value="${schoolInstance.numPeriods}"
		required="" class="form-control" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: schoolInstance, field: 'lunchPeriodStart', 'error')} required">
	<label for="lunchPeriodStart"> <g:message
			code="school.lunchPeriodStart.label" default="Lunch Period Start" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="lunchPeriodStart"
		value="${schoolInstance.lunchPeriodStart}" required=""
		class="form-control" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: schoolInstance, field: 'lunchPeriodEnd', 'error')} required">
	<label for="lunchPeriodEnd"> <g:message
			code="school.lunchPeriodEnd.label" default="Lunch Period End" /> <span
		class="required-indicator">*</span>
	</label>
	<g:textField name="lunchPeriodEnd"
		value="${schoolInstance.lunchPeriodEnd}" required=""
		class="form-control" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: schoolInstance, field: 'scheduleBlocks', 'error')} ">
	<label for="scheduleBlocks"> <g:message
			code="school.scheduleBlocks.label" default="Schedule Blocks ex:" />&lt;period;days&gt;

	</label>
	<div id="scheduleBlockFormContainer">
		<g:if test="${!schoolInstance.academicYearSchedules?.isEmpty()}">
			<g:each in="${schoolInstance.academicYearSchedules}" var="y">
				<div>
					<div class="row scheduleBlock">
						<div class="col-md-3">
							<g:textField name="years" class="form-control"
								placeholder="Year" value="${y.year }" />
						</div>
						<div class="col-md-7">
							<g:textArea name="scheduleBlocks" class="form-control"
								placeholder="Schedule Blocks" value="${y.scheduleBlocksString }" />
						</div>
						<div class="col-md-2">
							<a href="javascript:void(0);" class="btn btn-danger"
								onclick="removeYear(this);"><i class="fa fa-minus"></i></a>
						</div>
					</div>
				</div>
			</g:each>
		</g:if>
		<div class="scheduleBlockRow" style="display: none;">
			<div class="row scheduleBlock">
				<div class="col-md-3">
					<g:textField name="years" class="form-control"
						placeholder="Year" />
				</div>
				<div class="col-md-7">
					<g:textArea name="scheduleBlocks" class="form-control"
						placeholder="Schedule Blocks" />
				</div>
				<div class="col-md-2">
					<a href="javascript:void(0);" class="btn btn-danger removeYearBtn"
						onclick="removeYear(this);"><i class="fa fa-minus"></i></a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a href="javascript:void(0);" id="addYearBtn" class="pull-right"><i
				class="fa fa-plus fa-lg"></i> Add Academic Year</a>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$("#addYearBtn").click(
					function() {
						$("#scheduleBlockFormContainer").append($(".scheduleBlockRow").html())
					});
		});
		function removeYear(e) {
			if ($(".scheduleBlock").length > 1)
				$(e).closest(".scheduleBlock").remove();
		}
	</script>
</div>

