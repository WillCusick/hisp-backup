
<%@ page import="com.administration.School"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="admin">
<g:set var="entityName"
	value="${message(code: 'school.label', default: 'School')}" />
<title>Schools</title>
</head>
<body>
	<h1>Schools</h1>
	<div class="row">
		<div class="col-md-12">
			<g:link controller="school" action="create"
				class="btn btn-primary pull-right">Add School</g:link>
		</div>
		<div class="col-md-12">
			<g:if test="${flash.message}">
				<div class="alert alert-success" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.error}">
				<div class="alert alert-danger" role="status">
					${flash.error}
				</div>
			</g:if>
			<table class="table table-striped">
				<thead>
					<tr>
						<g:sortableColumn property="name"
							title="${message(code: 'school.name.label', default: 'School Name')}" />

						<g:sortableColumn property="numSemesters"
							title="${message(code: 'school.numSemesters.label', default: 'Number of Semesters')}" />

						<g:sortableColumn property="numDays"
							title="${message(code: 'school.numDays.label', default: 'Number of days')}" />

						<g:sortableColumn property="numPeriods"
							title="${message(code: 'school.numPeriods.label', default: 'Number of Periods')}" />

						<g:sortableColumn property="lunchPeriodStart"
							title="${message(code: 'school.lunchPeriodStart.label', default: 'Lunch start period')}" />

						<g:sortableColumn property="lunchPeriodEnd"
							title="${message(code: 'school.lunchPeriodEnd.label', default: 'Lunch end period')}" />

						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<g:each in="${schoolInstanceList}" status="i" var="schoolInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<td>
								${fieldValue(bean: schoolInstance, field: "name")}
							</td>

							<td>
								${fieldValue(bean: schoolInstance, field: "numSemesters")}
							</td>

							<td>
								${fieldValue(bean: schoolInstance, field: "numDays")}
							</td>


							<td>
								${fieldValue(bean: schoolInstance, field: "numPeriods")}
							</td>

							<td>
								${fieldValue(bean: schoolInstance, field: "lunchPeriodStart")}
							</td>

							<td>
								${fieldValue(bean: schoolInstance, field: "lunchPeriodEnd")}
							</td>

							<td><g:link controller="school" action="edit"
									id="${schoolInstance.id}" class="btn btn-xs btn-warning"
									title="Edit school">
									<i class="fa fa-edit fa-lg"></i>
								</g:link> <g:link controller="school" action="delete"
									id="${schoolInstance.id}"
									class="btn btn-xs btn-danger action-btn"
									title="Remove school">
									<i class="fa fa-trash-o fa-lg"></i>
								</g:link></td>
						</tr>
					</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${schoolInstanceCount ?: 0}" />
			</div>
		</div>
	</div>
</body>
</html>
