
<%@ page import="com.administration.School" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'school.label', default: 'School')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-school" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-school" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list school">
                                <g:if test="${schoolInstance?.name}">
                                <li class="fieldcontain">
                                    <span id="name-label" class="property-label"><g:message code="school.name.label" default="Name"/></span>
                                        <span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${schoolInstance}" field="numSemesters"/></span>
                                    
				<g:if test="${schoolInstance?.numSemesters}">
				<li class="fieldcontain">
					<span id="numSemesters-label" class="property-label"><g:message code="school.numSemesters.label" default="Num Semesters" /></span>
					
						<span class="property-value" aria-labelledby="numSemesters-label"><g:fieldValue bean="${schoolInstance}" field="numSemesters"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schoolInstance?.numDays}">
				<li class="fieldcontain">
					<span id="numDays-label" class="property-label"><g:message code="school.numDays.label" default="Num Days" /></span>
					
						<span class="property-value" aria-labelledby="numDays-label"><g:fieldValue bean="${schoolInstance}" field="numDays"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schoolInstance?.numPeriods}">
				<li class="fieldcontain">
					<span id="numPeriods-label" class="property-label"><g:message code="school.numPeriods.label" default="Num Periods" /></span>
					
						<span class="property-value" aria-labelledby="numPeriods-label"><g:fieldValue bean="${schoolInstance}" field="numPeriods"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schoolInstance?.lunchPeriodStart}">
				<li class="fieldcontain">
					<span id="lunchPeriodStart-label" class="property-label"><g:message code="school.lunchPeriodStart.label" default="Lunch Period Start" /></span>
					
						<span class="property-value" aria-labelledby="lunchPeriodStart-label"><g:fieldValue bean="${schoolInstance}" field="lunchPeriodStart"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schoolInstance?.lunchPeriodEnd}">
				<li class="fieldcontain">
					<span id="lunchPeriodEnd-label" class="property-label"><g:message code="school.lunchPeriodEnd.label" default="Lunch Period End" /></span>
					
						<span class="property-value" aria-labelledby="lunchPeriodEnd-label"><g:fieldValue bean="${schoolInstance}" field="lunchPeriodEnd"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schoolInstance?.scheduleBlocks}">
				<li class="fieldcontain">
					<span id="scheduleBlocks-label" class="property-label"><g:message code="school.scheduleBlocks.label" default="Schedule Blocks" /></span>
					
						<g:each in="${schoolInstance.scheduleBlocks}" var="s">
						<span class="property-value" aria-labelledby="scheduleBlocks-label"><g:link controller="scheduleBlock" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:schoolInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${schoolInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
