<%@ page import="com.administration.School"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="admin">
<g:set var="entityName"
	value="${message(code: 'school.label', default: 'School')}" />
<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
	<div id="edit-school" class="content scaffold-edit" role="main">
		<h1>
			<g:message code="default.edit.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="alert alert-success" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:if test="${flash.error}">
			<div class="alert alert-danger" role="status">
				${flash.error}
			</div>
		</g:if>
		<g:form url="[action:'update']" method="PUT">
			<g:hiddenField name="version" value="${schoolInstance?.version}" />
			<g:hiddenField name="id" value="${schoolInstance.id}" />
			<fieldset class="form">
				<g:render template="form" />
			</fieldset>
			<fieldset class="buttons">
				<g:actionSubmit class="save" action="update"
					value="${message(code: 'default.button.update.label', default: 'Update')}"
					class="btn btn-primary" />
			</fieldset>
		</g:form>
	</div>
</body>
</html>
