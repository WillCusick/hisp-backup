<html>
<head>
<meta name='layout' content='main' />
<title><g:message code='spring.security.ui.register.title' /></title>
<style type="text/css">
#registerformContainer {
	margin-left: auto;
	margin-right: auto;
	width: 80%;
}
</style>
</head>
<body>
	<div id="registerformContainer" class="row">
		<h2>Request Account</h2>
		<g:if test='${success}'>
			<br>
			<div>Account request has been sent.</div>
		</g:if>
		<g:else>
			<div class="row">
				<g:if test="${errors}">
					<br>
					<div class="alert alert-danger">
						<h3>Errors</h3>
						<ul>
							<g:each in="${errors}" var="error">
								<li>${error}</li>
							</g:each>
						</ul>
					</div>
				</g:if>
			</div>
			<form action='register' name='registerForm' class="form-horizontal">
				<div class="col-md-8">
					<div class="form-group">
						<label for="username" class="col-md-4 control-label">
							Username: <span class="required-indicator">*</span>
						</label>
						<div class="col-md-8">
							<input type="text" name="username" class="form-control"
								value="${formValues?.username}" id="username" required>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-md-4 control-label">
							Email: <span class="required-indicator">*</span>
						</label>
						<div class="col-md-8">
							<input type="email" name="email" class="form-control"
								value="${formValues?.email}" id="email" required>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-md-4 control-label">
							Password: <span class="required-indicator">*</span>
						</label>
						<div class="col-md-8">
							<input type="password" name="password" class="form-control" 
							id="password" required>
						</div>
					</div>
					<div class="form-group">
						<label for="password2" class="col-md-4 control-label">
							Confirm Password: <span class="required-indicator">*</span>
						</label>
						<div class="col-md-8">
							<input type="password" name="password2" class="form-control"
							id="password2" required>
						</div>
					</div>
					<div class="form-group">
						<label for="firstName" class="col-md-4 control-label">
							First Name: <span class="required-indicator">*</span>
						</label>
						<div class="col-md-8">
							<input type="text" name="firstName" class="form-control"
							value="${formValues?.firstName}" id="firstName" required>
						</div>
					</div>
					<div class="form-group">
						<label for="lastName" class="col-md-4 control-label">
							lastName: <span class="required-indicator">*</span>
						</label>
						<div class="col-md-8">
							<input type="text" name="lastName" class="form-control"
							value="${formValues?.lastName}" id="lastName" required>
						</div>
					</div>
					<div class="form-group">
						<label for="school" class="col-md-4 control-label">
							School: <span class="required-indicator">*</span>
						</label>
						<div class="col-md-8">
							<select name="school" class="form-control">
								<option value="-1">Select Your School</option>
								<g:each in="${schools}" var="s">
									<option value="${s.id}"
										${formValues?.school==s.id.toString()?'selected':''}>${s}
									</option>
								</g:each>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-4 col-md-8">
							<g:submitButton name="Submit" class="btn btn-primary pull-right" />
						</div>
					</div>
				</div>
			</form>
		</g:else>
	</div>
	<script>
	$(document).ready(function() {
		$('#username').focus();
	});
	</script>
</body>
</html>
