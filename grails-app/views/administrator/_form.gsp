<%@ page import="com.administration.Administrator" %>



<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="administrator.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${administratorInstance?.username}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="administrator.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" required="" value="${administratorInstance?.password}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'enabled', 'error')} ">
	<label for="enabled">
		<g:message code="administrator.enabled.label" default="Enabled" />
		
	</label>
	<g:checkBox name="enabled" value="${administratorInstance?.enabled}" />

</div>

