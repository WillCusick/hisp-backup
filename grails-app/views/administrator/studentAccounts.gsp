
<%@ page import="com.student.Student"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="admin">
<g:set var="entityName"
	value="${message(code: 'student.label', default: 'Student Account')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div id="list-student" class="content scaffold-list" role="main">
		<h1>
			<g:message code="default.list.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="alert alert-success" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:if test="${flash.error}">
			<div class="alert alert-danger" role="status">
				${flash.error}
			</div>
		</g:if>
		<table class="table table-striped">
			<thead>
				<tr>

					<g:sortableColumn property="username"
						title="${message(code: 'student.username.label', default: 'Username')}" />

					<g:sortableColumn property="firstName"
						title="${message(code: 'student.firstName.label', default: 'First Name')}" />

					<g:sortableColumn property="lastName"
						title="${message(code: 'student.lastName.label', default: 'Last Name')}" />

					<g:sortableColumn property="email"
						title="${message(code: 'student.email.label', default: 'Email')}" />

					<th><g:message code="student.school.label" default="School" /></th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${studentInstanceList}" status="i" var="studentInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td>${fieldValue(bean: studentInstance, field: "username")}</td>

						<td>
							${fieldValue(bean: studentInstance, field: "firstName")}
						</td>

						<td>
							${fieldValue(bean: studentInstance, field: "lastName")}
						</td>

						<td>
							${fieldValue(bean: studentInstance, field: "email")}
						</td>

						<td>
							${fieldValue(bean: studentInstance, field: "school")}
						</td>
						<td><g:link controller="administrator" action="deleteStudent"
								id="${studentInstance.id}" class="btn btn-xs btn-danger action-btn" title="Delete student">
								<i class="fa fa-trash-o fa-lg"></i>
							</g:link></td>
					</tr>
				</g:each>
			</tbody>
		</table>
		<div class="pagination">
			<g:paginate total="${studentInstanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
