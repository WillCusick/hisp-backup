
<%@ page import="com.security.AccountRequest"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="admin">
<g:set var="entityName"
	value="${message(code: 'accountRequest.label', default: 'AccountRequest')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div id="list-accountRequest" class="content scaffold-list" role="main">
		<h1>Student Account Requests</h1>
		<g:if test="${flash.message}">
			<div class="alert alert-success" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:if test="${flash.error}">
			<div class="alert alert-danger" role="status">
				${flash.error}
			</div>
		</g:if>
		<table class="table table-striped">
			<thead>
				<tr>
					<g:sortableColumn property="student.school.name"
						title="${message(code: 'accountRequest.student.school.name.label', default: 'School Name')}" />

					<g:sortableColumn property="student.firstName"
						title="${message(code: 'accountRequest.student.firstName.label', default: 'First Name')}" />

					<g:sortableColumn property="student.lastName"
						title="${message(code: 'accountRequest.student.lastName.label', default: 'Last Name')}" />


					<g:sortableColumn property="student.username"
						title="${message(code: 'accountRequest.student.username.label', default: 'Username')}" />

					<g:sortableColumn property="student.email"
						title="${message(code: 'accountRequest.student.email.label', default: 'Email')}" />

					<g:sortableColumn property="dateTime"
						title="${message(code: 'accountRequest.dateTime.label', default: 'Date')}" />
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${accountRequestInstanceList}" status="i"
					var="accountRequestInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>
							${fieldValue(bean: accountRequestInstance, field: "student.school.name")}
						</td>

						<td>
							${fieldValue(bean: accountRequestInstance, field: "student.firstName")}
						</td>

						<td>
							${fieldValue(bean: accountRequestInstance, field: "student.lastName")}
						</td>


						<td>
							${fieldValue(bean: accountRequestInstance, field: "student.username")}
						</td>

						<td>
							${fieldValue(bean: accountRequestInstance, field: "student.email")}
						</td>

						<td><g:formatDate date="${accountRequestInstance.dateTime}"
								format="MM/dd/yyyy hh:mm aa" /></td>

						<td><g:link controller="administrator"
								action="approveRequest" id="${accountRequestInstance.id}"
								class="btn btn-xs btn-success action-btn btn-icon"
								title="Approve request">
								<i class="fa fa-check fa-lg"></i>
							</g:link> <g:link controller="administrator" action="rejectRequest"
								id="${accountRequestInstance.id}"
								class="btn btn-xs btn-danger action-btn btn-icon"
								title="Reject request">
								<i class="fa fa-times fa-lg"></i>
							</g:link></td>
					</tr>
				</g:each>
			</tbody>
		</table>
		<div class="pagination">
			<g:paginate total="${accountRequestInstanceCount ?: 0}" />
		</div>
		<div>
			<g:link controller="administrator"
				action="approveAllRequests"
				class="btn btn-default btn-success action-btn"
				title="Approve all requests">
				<span>Approve All Requests</span>
			</g:link>
		</div>
	</div>
</body>
</html>
