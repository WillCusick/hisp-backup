
<%@ page import="com.administration.Administrator"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="admin">
<g:set var="entityName"
	value="${message(code: 'administrator.label', default: 'Administrator')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<h2>Administrator Dashboard</h2>
	<div>
		<h3>Account Requests: ${accountRequestCount}</h3>
	</div>
</body>
</html>
