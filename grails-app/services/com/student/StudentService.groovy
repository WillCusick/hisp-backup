package com.student

import com.security.SecRole;
import com.security.User
import com.security.UserSecRole

import grails.transaction.Transactional

@Transactional
class StudentService {

	def deleteStudent(Student s) {
		if(!s)
			throw new Exception("Student account could not be located")
		if(!s.enabled)
			throw new Exception("Student account not enabled")
		def roles = UserSecRole.remove(s, SecRole.findWhere(authority: 'ROLE_STUDENT'),true);//find all security roles for student account
		for(f in s.friends)
			f.removeFromFriends(s)
		s.friends.clear()
		s.friendRequests.clear()
		def req=Student.withCriteria {
			friendRequests {
				idEq(s.id)
			}
		 }
		req*.removeFromFriendRequests(s)
		
		for(schedule in s.assignedSchedules)
		{
			schedule.courses.clear()
			schedule.save()
		}
		
		if(roles)
			s.delete()
		else{
			s.enabled=false
			s.save()
		}
		//		if (((User)s).delete(flush:true))//delete student account from database
		log.info "Student deleted"
		return true
	}
}
