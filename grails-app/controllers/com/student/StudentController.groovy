package com.student

import static org.springframework.http.HttpStatus.*

import com.administration.School;

import grails.transaction.Transactional

@Transactional(readOnly = true)
class StudentController {
	def springSecurityService;
	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	/**
	 * Action to display a student's dashboard
	 *
	 * @return the student's dashboard
	 */
	def index() {
		render view:'index';
	}

	/**
	 * Action to display a student's account
	 *
	 * @return the student's account
	 */
	def myAccount(){
		log.info "myAccount entered"
		// Get current user
		def user = (Student)springSecurityService.currentUser
		render view:'myAccount',model:[student:user]
	}

	/**
	 * Action to update a student's account with the information passed in
	 *
	 * @param password (optional) the new password
	 * @param passwordConfirm (optional) the new password. must equal password
	 * @param email (optional) the new email
	 * @param firstName (optional) the new first name
	 * @param lastName (optional) the new last name
	 * @param school (optional) the new school
	 * @return the student's dashboard with any confirmation or error messages
	 */
	@Transactional
	def updateAccount(){
		log.info "updateAccount entered"
		try{
			// Get current user
			def user = (Student)springSecurityService.currentUser

			if(!params.password?.trim()?.isEmpty() || 
				!params.passwordConfirm?.trim()?.isEmpty()){
				user.password = params.password
				log.info "update password"
			}

			def errors = [];
			if(!user.email.equalsIgnoreCase(params.email?.trim())){
				String email = params.email?.trim()
				if(Student.findWhere(email:email))
					errors.add("Email is already associated with an account")
				else{
					user.email = email
					log.info "update email"
				}
			}

			String fName = params.firstName?.trim()
			if(fName.isEmpty())
				errors.add("First name must be entered")
			else {
				user.firstName = fName
				log.info "update first name"
			}
			
			String lName = params.lastName?.trim()
			if(lName.isEmpty())
				errors.add("Last name must be entered")
			else {
				user.lastName=lName
				log.info "update last name"
			}

			def s = params.school.id
			user.school = School.get(params.school.id as long)
			log.info "update school"

			if(!errors.isEmpty()){
				flash.errors=errors
				log.info "Validation errors: ${errors}"
				log.info "update invalid"
				render view:'myAccount',model:[student:user]
			} else {
				log.info "update successful"
				user.save(flush:true)
				log.info "save user to database"
				flash.message="Account updated"
				redirect action:'myAccount'
			}
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			flash.error=e.getMessage()
			redirect action:'myAccount'
		}
	}
}
