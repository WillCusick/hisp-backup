package com.student

class FriendsController {
	def springSecurityService
	
	/**
	 * Action to display a user's friends
	 *
	 * @return list of user's friend sort by last name then first name
	 */
	def myFriends() {
		log.info "myFriends entered"
		def friendList = []
		// Get current user from spring security service
		def user = (Student)springSecurityService.currentUser

		// Get user's friends sorted by last name then first name
		friendList = user.friends.sort{
			[it.lastName, it.firstName]
		}.reverse()
		log.info "friends list created"
		render view:'myFriends',model:[friends:friendList]
	}

	/**
	 * Action to unfriend a friend
	 * @param friend the student to unfriend
	 *
	 * @return redirect to friends list
	 */
	def unfriend(Student friend){
		log.info "unfriend entered"
		try{
			if (!friend)
				throw new Exception("Friend not found")

			// Get current user from spring security service
			def user = (Student)springSecurityService.currentUser

			// remove from each other's friends list
			user.removeFromFriends(friend)
			friend.removeFromFriends(user)

			// save changes to database
			user.save(flush:true)
			friend.save(flush:true)
			log.info "Unfriend successful"
			flash.message = "${friend.firstName} ${friend.lastName} removed from friends"
			redirect action:'myFriends'
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			flash.error = e.getMessage()
			redirect action:'myFriends'
		}
	}

	def manageFriends(){
	}

	/**
	 * Ajax action to search for student's that match the name search query 
	 *
	 * @param name first and last name of the student requested
	 * @return list of search results that match the name
	 */
	def searchFriends(){
		log.info "unfriend entered"
		try{
			// Get current user from spring security service
			def user = (Student)springSecurityService.currentUser

			String query = params.name
			if(query?.split(" ")?.length<2) {
				log.info "First and last name not entered"
				render "First and last name must be entered"
				return
			}

			String fName = query?.split(" ")[0]?.trim()
			String lName = query?.substring(query.indexOf(' '))?.trim()

			def studentList = []
			def studentMap = [:]
			studentList = Student.findAllWhere(firstName:fName,lastName:lName,enabled:true)
			// Determine state to describe user's current relationship with other student s
			for(s in studentList){
				// Default relationship is stranger
				String state = "stranger"
				if(user.friendRequests.contains(s)) // User has a request from student s
					state="recieved"
				else if(s.friendRequests.contains(user)) // User sent a friend request to student s
					state="sent"
				else if(user.friends.contains(s)) // User is already friend with student s
					state="friend"
				studentMap.put(s,state)
			}
			log.info "Generated search result list"

			if(studentList.isEmpty()){
				log.info "No matching student found"
				render "No results"
			} else {
				log.info "Matching student(s) found"
				render (template:'/friends/friend_search_result_list',model:[result:studentMap])
			}
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			render e.getMessage()
		}
	}

	/**
	 * Ajax action to send a friend request to another student
	 *
	 * @param s the student to send a friend request to
	 * @return error message if failed, otherwise "Friend request sent"
	 */
	def sendFriendRequest(Student s){
		log.info "sendFriendRequest"
		try{
			if(!s || !s.enabled)
				throw new Exception("Student could not be located")

			// Get current user
			def user = (Student)springSecurityService.currentUser
			if(s.friendRequests?.contains(user))
				throw new Exception("Friend request already sent");

			// Add current user to Student s's friend request list
			s.friendRequests.add(user)
			log.info "Current user added to student's friend request list"
			s.save(flush:true)
			render "Friend request sent";
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			render e.getMessage()
		}
	}

	/**
	 * Ajax action to accept a friend request
	 *
	 * @param s the student whose friend request to accept
	 * @return error message if failed, otherwise "Friend request accepted"
	 */
	def acceptFriendRequest(Student s){
		log.info "acceptFriendRequest"
		try{
			if(!s || !s.enabled)
				throw new Exception("Student could not be located")

			// Get current user
			def user = (Student)springSecurityService.currentUser

			if(user.friends.contains(s)){
				// Student is already a friend. Remove friend request
				log.info "Student already is friend"
				user.friendRequests.remove(s)
				log.info "Friend request removed"
			} else {
				user.friends.add(s)
				log.info "Student added to friends"
				s.friends.add(user)
				log.info "User added to Student's friends"
				user.friendRequests.remove(s)
				log.info "Friend request removed"
			}
			render "Friend request accepted"
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			render e.getMessage()
		}
	}

	/**
	 * Ajax action to decline a friend request
	 *
	 * @param s the student whose friend request to decline
	 * @return error message if failed, otherwise "Friend request declined"
	 */
	def declineFriendRequest(Student s){
		log.info "declineFriendRequest entered"
		try{
			if(!s || !s.enabled)
				throw new Exception("Student could not be located")

			// Get current user
			def user = (Student)springSecurityService.currentUser

			if(user.friends.contains(s)){
				// Student is already a friend. Remove friend request
				log.info "Student already is friend"
				user.friendRequests.remove(s)
				log.info "Friend request removed"
			} else {
				user.friendRequests.remove(s)
				log.info "Friend request declined and removed"
			}
			render "Friend request declined"
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			render e.getMessage()
		}
	}

	/**
	 * Action to view the user's friend requests
	 *
	 * @return list of the user's friend requests
	 */
	def requests(){
		log.info "requests entered"
		try{
			// Get current user
			def user = (Student)springSecurityService.currentUser

			// Get user's friend requests sorted by last name, first name
			def requests=user.friendRequests.sort{
				[it.lastName, it.firstName]
			}.reverse()
			render view:'requests',model:[requests:requests]
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			render e.getMessage()
		}
	}
}
