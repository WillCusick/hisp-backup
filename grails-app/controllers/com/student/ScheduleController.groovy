package com.student

import com.schedule.CourseSection;
import com.schedule.DesiredScheduleCourse;
import com.schedule.DesiredScheduleCriteria
import com.schedule.DesiredScheduleFactory
import com.schedule.Schedule;
import com.schedule.Course;
import com.administration.ScheduleBlock;
import com.administration.AcademicYearScheduleBlockSet;
import com.administration.School;

import grails.transaction.Transactional
import static org.springframework.http.HttpStatus.*
import grails.converters.JSON

@Transactional(readOnly = true)
class ScheduleController {
	def springSecurityService;
	private static final MIN_YEAR=2013
	private static final COURSE_SEEN_THIS_YEAR = 1
	private static final COURSE_SEEN_PREV_YEAR = 0
	private static final COURSE_SEEN_NEVER = -1
	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	/**
	 * Method that sorts course sections by the following criteria:
	 * <p>
	 * <ul>
	 * <li> Compares periods
	 * <li> If the periods are the same, compare days
	 * <li> If the days are the same, compare course identities
	 * <ul>
	 * <p>
	 *
	 * @param sections the section to sort
	 * @return the sorted section
	 */
	private def sortSections(Object[] sections) {
		// Note: sections is Object[] because that is what groovy coerces .toArray() to
		log.info "Sorting course sections"
		return sections?.sort{ a,b ->
			a.block.periods <=> b.block.periods ?:
			a.block.days <=> b.block.days ?:
			a.course.identity <=> b.course.identity
		}
	}

	/**
	 * Action to view assigned schedule. If the user has an assigned schedule,
	 * the most recent one is shown by default
	 *
	 * @return the student's assigned schedule, listing the course sections
	 */
	def assigned() {
		log.info "assigned entered"
		def user = (Student)springSecurityService.currentUser

		// Preload some data for the form
		int currentYear = Calendar.getInstance().get(Calendar.YEAR)
		int maxYear = currentYear + 2
		int maxSemester = user.school.numSemesters
		int days = user.school.numDays
		int periods = user.school.numPeriods

		// Find out if the user has any schedules
		def assignedSchedules = user.assignedSchedules?.toArray()
		def scheduleExists = (assignedSchedules != null && assignedSchedules.length != 0)

		if (scheduleExists) {
			// If his schedule exists, show his most recent one
			def mostRecentSchedule = assignedSchedules[0]
			for (schedule in assignedSchedules) {
				if (schedule.year >= mostRecentSchedule.year &&
				schedule.semester > mostRecentSchedule.semester)
					mostRecentSchedule = schedule
			}

			def year = mostRecentSchedule.year
			def semester = mostRecentSchedule.semester
			log.info "showing most recent assigned schedule"
			render view:'assigned', model:[scheduleExists:true, schedule:mostRecentSchedule,
				year:year, semester:semester, minYear:MIN_YEAR,maxYear:maxYear,
				currentYear:currentYear, maxSemester:maxSemester, days:days,
				periods:periods]
		} else {
			// If his schedule doesn't exist, ask him to select a schedule
			render view:'assigned', model:[scheduleExists:false, minYear:MIN_YEAR,
				maxYear:maxYear, currentYear:currentYear, maxSemester:maxSemester,
				days:days, periods:periods]
		}
	}

	/**
	 * AJAX Action to view an assigned schedule.
	 * Displays the student's course, sorted by sortSections,
	 * with the ability to remove or add courses
	 *
	 * @return the student's assigned schedule, listing the course sections
	 */
	def getAssignedScheduleEditForm(int year, int semester, boolean showFriends) {
		log.info "getAssignedScheduleEditForm entered"
		try{
			def user = (Student)springSecurityService.currentUser
			def assignedSchedules = user.assignedSchedules.toArray()

			if (year < MIN_YEAR)
				throw new Exception("Invalid year $year")

			if (semester < 1 || semester > user.school.numSemesters)
				throw new Exception("Invalid semester $semester")

			log.info "Finding requested schedule"
			Schedule s = assignedSchedules.find{
				it.year == year && it.semester == semester && it.id != user.desiredSchedule?.id
			}
			if(!s){
				log.info "Schedule not found"
				s = new Schedule(year:year, semester:semester)
				user.addToAssignedSchedules(new Schedule(year:year, semester:semester))
				s.save()
				user.save()
			}

			def sections = s?.courses?.toArray()
			sections = sortSections(sections)

			def friendsInCourses = [:]
			if (showFriends) {
				log.info "Getting user's friends in the same school"
				def usersFriends = user?.friends?.toArray().findAll{
					it.school == user.school
				}

				log.info "Getting friends' schedules from the same semester"
				def friendsSchedulesList = usersFriends?.assignedSchedules?.flatten()
				def friendsSchedules = friendsSchedulesList?.toArray()?.findAll{
					it.year == year && it.semester == semester && it.student?.desiredSchedule?.id  != it.id
				}

				log.info "Mapping friends and courses"
				for (section in sections) {
					def friends = friendsSchedules?.findAll{
						section in it.courses
					}.collect{
						it.student
					}
					friendsInCourses[section] = friends.collect { it.firstName + " " + it.lastName}
				}
			}

			log.info "render form"
			render template:'/schedule/edit_assigned_schedule_form',model:[schedule:s, courses:sections,
				showFriends:showFriends, friends: friendsInCourses]
		}
		catch(Exception e){
			log.error "Exception occurred"
			log.error e.stackTrace.toString()
			response.status=500
			render e.getMessage()
		}
	}

	/**
	 * AJAX Action to remove a course from all assigned schedule.
	 *
	 * @return HTTP 500 if the course could not be removed, otherwise 200
	 */
	@Transactional
	def removeFromSchedule(long courseSectionId){
		log.info "removeFromSchedule entered"
		try{
			def user = (Student)springSecurityService.currentUser
			def section = CourseSection.get(courseSectionId)
			if (!section)
				throw new Exception("Course section $courseSectionId could not be found")

			log.info "Finding schedules"
			def schedules = user.assignedSchedules?.toArray()?.findAll{
				it.student.desiredSchedule?.id  != it.id && section in it.courses
			}
			for (schedule in schedules)
				removeCourseFromSchedule(schedule, section)
			render "Course removed"
		}
		catch(Exception e){
			log.error "Exception occurred"
			log.error e.stackTrace.toString()
			response.status = 500
			render e.getMessage()
		}
	}

	private def removeCourseFromSchedule(Schedule schedule, CourseSection section) throws Exception {
		log.info "removeCourseFromSchedule entered with schedule ${schedule?.id} and course ${section?.id}"

		if(!schedule?.courses?.contains(section)) {
			log.warn "Course ${section?.id} not found for schedule ${schedule?.id}"
			throw new Exception("Course section could not be removed")
		}

		log.info "Course ${section?.id} removed from schedule ${schedule?.id}"
		schedule.courses.remove(section)
		schedule.save(flush:true)
	}

	/**
	 * AJAX Action to validate if a course exists for a given year,
	 * courseId and the user's school.
	 * <p>
	 * The JSON object returned has the following properties:
	 * <ul>
	 * <li>courseTitle: title of the course (if it exists)
	 * <li>seenThisYear: if the courseId has been seen for this school this year
	 * <li>seenPrevYears: if the courseId has been seen for this school a prev year
	 * <ul>
	 * seenThisYear and seenPrevYears will never both be true
	 * <p>
	 *
	 * @param year the year (in YYYY format)
	 * @param courseId the (unique) string identifying the course for the school
	 * @return JSON object described above
	 */
	def validateCourse(int year, String courseId) {
		log.info "Validating course $courseId"
		try {
			if (year < MIN_YEAR)
				throw new Exception("Invalid year $year")

			def result = [:]

			def user = (Student)springSecurityService.currentUser
			def course = Course.findWhere(school:user.school, identity:courseId)

			def courseSeenStatus = courseSeen(year, course)

			result['courseTitle'] = course?.title
			result['seenThisYear'] = (courseSeenStatus == COURSE_SEEN_THIS_YEAR)
			result['seenPrevYears'] = (courseSeenStatus == COURSE_SEEN_PREV_YEAR)
			
			log.info "Returning course validation JSON"
			render result as JSON
		}
		catch(Exception e){
			log.error "Exception occurred"
			log.error e.stackTrace.toString()
			response.status = 500
			render e.getMessage()
		}
	}

	/**
	 * Private helper method to check when a course was most recently seen
	 *
	 * @param year the year (in YYYY format)
	 * @param course the course to search for
	 * @return the status of when a course was recently seen
	 */
	private def courseSeen(int year, Course course) {
		log.info "Checking if course $course was seen in the year $year"

		def sections = course?.sections?.toArray()
		def result = COURSE_SEEN_NEVER
		for (section in sections) {
			def sectionYear = section.block.schoolAcademicYear.year

			// Check if this section is recent
			if (sectionYear == year) {
				log.info "Course seen this year"
				return COURSE_SEEN_THIS_YEAR
			} else if (sectionYear < year) {
				log.info "Course seen in a previous year"
				result = COURSE_SEEN_PREV_YEAR
			}
		}

		return result
	}

	/**
	 * AJAX Action to update a course title.
	 * Note: this change is retroactive and will change titles of courses
	 * from previous years and semesters.
	 *
	 * @param year the year (in YYYY format)
	 * @param courseId the (unique) string identifying the course for the school
	 * @param courseTitle the new courseTitle. must be non-empty
	 * @return JSON with result true if the operation succeeded, false otherwise
	 */
	@Transactional
	def updateCourseTitle(int year, String courseId, String courseTitle) {
		log.info "Updating course '$courseId' with title '$courseTitle'"
		try {
			if (year < MIN_YEAR)
				throw new Exception("Invalid year $year")

			def user = (Student)springSecurityService.currentUser
			def course = Course.findWhere(school:user.school, identity:courseId)

			def result = [result:false]
			if (course == null) {
				log.info "Course not found: creating course '$courseId' with title '$courseTitle'"
				course = new Course(identity:courseId,title:courseTitle,school:user.school)
				course.save()
				result['result'] = true
				render result as JSON
			} else if (courseTitle.trim() != "" && courseTitle != course.title &&
				courseSeen(year, course) == COURSE_SEEN_PREV_YEAR) {
				log.info "Course found in a previous year: updating course '$courseId' with title '$courseTitle'"
				course.title = courseTitle.trim()
				course.save()
				result['result'] = true
				render result as JSON
			}
			render result as JSON
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			response.status = 500
			render e.getMessage()
		}
	}

	/**
	 * AJAX Action to validate that a schedule block is legal and get
	 * teacher's name
	 * <p>
	 * The JSON object returned has the following properties:
	 * <ul>
	 * <li>sectionSeen: if the course has been seen for the schedule block
	 * <li>teacher: the name of the teacher (if it exists) otherwise empty string
	 * <li>valid: if the schedule block described by days and periods is legal
	 * <ul>
	 * If valid is false, sectionSeen is false and teacher is empty.
	 * if sectionSeen is true, teacher is non-empty and valid is true
	 * <p>
	 *
	 * @param year the year (in YYYY format)
	 * @param semesterStart the starting semester (range from 1 to min of user's school numSemesters or semesterEnd)
	 * @param semesterEnd the ending semester (range from semesterStart to user's school numSemesters)
	 * @param days the days the class meets (multiple days delimited by , and in ascending order)
	 * @param periods the periods the class meets (multiple periods delimited by , and in ascending order)
	 * @param courseId the (unique) string identifying the course for the school
	 * @return JSON object described above
	 */
	def validateScheduleBlock(int year, int semesterStart, int semesterEnd, String days,
		String periods, String courseId) {
		log.info "Validating block for periods $periods, days $days"
		try {
			def user = (Student)springSecurityService.currentUser

			if (year < MIN_YEAR)
				throw new Exception("Invalid year $year")
			if (semesterStart < 1 || semesterStart > user.school.numSemesters ||
				semesterStart > semesterEnd || semesterEnd > user.school.numSemesters)
				throw new Exception("Invalid semester range $semesterStart - $semesterEnd")

			def result = [sectionSeen:false, teacher:'', valid:false]

			def course = Course.findWhere(school:user.school, identity:courseId)
			def block = getBlock(year, days, periods)

			if (block) {
				log.info "Block valid for periods $periods, days $days"
				result['valid'] = true
			} else {
				// If the schedule block isn't valid, return now
				log.error "Invalid block for periods $periods, days $days"
				render result as JSON
			}

			def teacher = getSectionTeacher(year, semesterStart, semesterEnd, block, course)
			if (teacher != null) {
				result['sectionSeen'] = true
				result['teacher'] = teacher
			}

			render result as JSON
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			response.status = 500
			render e.getMessage()
		}
	}

	/**
	 * Gets and returns the teacher of a course section for a specified year,
	 * semester range, schedule block, and course
	 *
	 * @param year the year (in YYYY format)
	 * @param semesterStart the starting semester (range from 1 to min of user's school numSemesters or semesterEnd)
	 * @param semesterEnd the ending semester (range from semesterStart to user's school numSemesters)
	 * @param block the schedule block
	 * @param course the course to search for
	 * @return the teacher if found, else null
	 */
	private def getSectionTeacher(int year, int semesterStart, int semesterEnd, 
		ScheduleBlock block, Course course) {
		log.info "Checking if a section for $course has been seen"

		def sections = course?.sections?.toArray()
		for (section in sections) {
			def sectionBlock = section.block
			def sectionYear = block.schoolAcademicYear.year
			def sectionSemesterStart = section.semesterStart
			def sectionSemesterEnd = section.semesterEnd
			
			if (sectionYear == year && sectionSemesterStart == semesterStart &&
				sectionSemesterEnd == semesterEnd && sectionBlock == block) {
				log.info "Course section seen for course $course"
				return section.teacher
			}
		}

		return null // Not found
	}

	/**
	 * AJAX Action to update an teachers name.
	 *
	 * @param year the year (in YYYY format)
	 * @param semesterStart the starting semester (range from 1 to min of user's school numSemesters or semesterEnd)
	 * @param semesterEnd the ending semester (range from semesterStart to user's school numSemesters)
	 * @param days the days the class meets (multiple days delimited by , and in ascending order)
	 * @param periods the periods the class meets (multiple periods delimited by , and in ascending order)
	 * @param courseId the (unique) string identifying the course for the school
	 * @param teacher the teacher's name
	 * @return HTTP 500 if operation failed otherwise 200
	 */
	@Transactional
	def updateTeacher(int year, int semesterStart, int semesterEnd, String days,
			String periods, String courseId, String teacher) {
		log.info "Updating $courseId with new teacher $teacher"
		try {
			if (teacher.trim() == "") {
				log.error "Empty teacher name"
				render (status:500, text:"Teacher name must not be empty")
			}

			def user = (Student)springSecurityService.currentUser

			if (year < MIN_YEAR)
				throw new Exception("Invalid year $year")
			if (semesterStart < 1 || semesterStart > user.school.numSemesters ||
				semesterStart > semesterEnd || semesterEnd > user.school.numSemesters)
				throw new Exception("Invalid semester range $semesterStart - $semesterEnd")

			def course = Course.findWhere(school:user.school, identity:courseId)
			def block = getBlock(year, days, periods)

			if (block) {
				log.info "Block valid for periods $periods, days $days"
			} else {
				log.error "Invalid block for periods $periods, days $days"
				render (status:500, text:"Illegal Schedule Block")
			}

			def sectionTeacher = getSectionTeacher(year, semesterStart, semesterEnd, block, course)
			if (!sectionTeacher) {
				// Section wasn't found, must create it
				def courseSection = new CourseSection(teacher:teacher, course:course,
				semesterStart:semesterStart, semesterEnd:semesterEnd, block:block)
				courseSection.save(flush:true)
				course.save(flush:true)
				render (status:200, text:"Teacher updated")
			} else {
				// Teacher found, no need to update
				render (status:200, text:"Teacher not updated")
			}
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			response.status = 500
			render e.getMessage()
		}
	}

	/**
	 * AJAX Action to add a course to assigned schedules.
	 * Adds the section to an assigned schedule for each semester the course section meets
	 *
	 * @param year the year (in YYYY format)
	 * @param semesterStart the starting semester (range from 1 to min of user's school numSemesters or semesterEnd)
	 * @param semesterEnd the ending semester (range from semesterStart to user's school numSemesters)
	 * @param days the days the class meets (multiple days delimited by , and in ascending order)
	 * @param periods the periods the class meets (multiple periods delimited by , and in ascending order)
	 * @param courseId the (unique) string identifying the course for the school
	 * @param teacher the teacher's name
	 * @return HTTP 500 if operation failed otherwise 200
	 */
	@Transactional
	def addCourseToAssigned(int year, int semesterStart, int semesterEnd, String days,
			String periods, String courseId, String teacher) {
		log.info "Adding course $courseId to assigned schedule"
		try {
			def user = (Student)springSecurityService.currentUser

			if (year < MIN_YEAR)
					throw new Exception("Invalid year $year")
			if (semesterStart < 1 || semesterStart > user.school.numSemesters ||
				semesterStart > semesterEnd || semesterEnd > user.school.numSemesters)
				throw new Exception("Invalid semester range $semesterStart - $semesterEnd")

			def course = Course.findWhere(school:user.school, identity:courseId)
			def block = getBlock(year, days, periods)

			if (block) {
				log.info "Block valid for periods $periods, days $days"
			} else {
				log.error "Invalid block for periods $periods, days $days"
				render (status:500, text:"Illegal Schedule Block")
			}

			def school = user.school
			if (semesterStart > semesterEnd || semesterStart < 1 ||
				semesterStart > school.numSemesters || semesterEnd > school.numSemesters) {
				log.error "Invalid range of semesters from $semesterStart to $semesterEnd"
				render (status:500, text:"Illegal Semester Range")
			}

			// Find the section the user is trying to add
			def section = CourseSection.findWhere(course:course, semesterStart:semesterStart,
				semesterEnd:semesterEnd, block:block)

			if (!section) {
				log.info "Section not found, creating section"
				// If we couldn't find the section, we must create it
				section = new CourseSection(teacher:teacher, course:course,
				semesterStart:semesterStart, semesterEnd:semesterEnd, block:block)
				section.save(flush:true)
				course.save(flush:true)
			}

			def assignedSchedules = user.assignedSchedules?.toArray()

			// Generate a map of schedules keyed by semester
			def schedules = [:]
			for (def semester = semesterStart; semester <= semesterEnd; semester++) {
				schedules[semester] = null
			}

			def semesters = schedules.keySet()
			for (schedule in assignedSchedules) {
				if (schedule.year == year && schedule.semester in semesters && schedule.id != user.desiredSchedule?.id) {
					log.info "Assigned schedule found for year: $year, semester: ${schedule.semester}"
					schedules[schedule.semester] = schedule
				}
			}

			def sectionWasInserted = false

			// For each schedule, add the course
			schedules.each{ semester, schedule ->
				// If it doesn't exist, create it!
				if (!schedule) {
					log.info "Creating assigned schedule for year: $year, semester: $semester"
					schedule = new Schedule(year:year, semester:semester)
					user.addToAssignedSchedules(schedule)
				}

				if (!(section in schedule.courses)) {
					// Only add the section if it is not already in the schedule
					log.info "Adding course $section for year $year, semester $semester"
					sectionWasInserted = true
					schedule.addToCourses(section)
					schedule.save(flush:true)
				}
			}

			println sectionWasInserted
			// We're done
			log.info "Course added for all semesters"
			user.save(flush:true)

			if (sectionWasInserted)
				render (status:200, text:"Course added")
			else
				render (status:500, text:"Course could not be added.<br>Already exists in the schedule");
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			response.status = 500
			render e.getMessage()
		}
	}

	/**
	 * Helper method to find a schedule block
	 *
	 * @param year the year (in YYYY format)
	 * @param days the days the class meets (multiple days delimited by , and in ascending order)
	 * @param periods the periods the class meets (multiple periods delimited by , and in ascending order)
	 * @return the schedule block if it is legal, otherwise null
	 */
	private def getBlock(int year, String days, String periods) {
		def user = (Student)springSecurityService.currentUser
		def blockSet = AcademicYearScheduleBlockSet.findWhere(school:user.school,
		year:year)
		def block

		for (scheduleBlock in blockSet?.scheduleBlocks) {
			if (scheduleBlock.periods == periods && scheduleBlock.days == days) {
				block = scheduleBlock
				break
			}
		}

		return block
	}

	/**
	 * Action to display a student's desired schedule, sorted by sortSections
	 * If it is not found, display links to generate form instead
	 *
	 * @return the desired schedule, sorted by sortSections
	 */
	def desired() {
		log.info "desired entered"
		def user = (Student)springSecurityService.currentUser;
		def sections=[]
		if(user.desiredSchedule){
			log.info "found desired schedule"
			Schedule s=user.desiredSchedule
			sections = s?.courses?.toArray()
			sections = sortSections(sections)
		}
        log.info "Getting user's friends in the same school"
        def usersFriends = user?.friends?.toArray().findAll{
                it.school == user.school
        }

        log.info "Getting friends' schedules from the same semester"
        def friendsSchedulesList = usersFriends?.assignedSchedules?.flatten()
        def friendsSchedules = friendsSchedulesList?.toArray()?.findAll{
                it.year == user.desiredSchedule?.year && it.semester == user.desiredSchedule?.semester && it.student?.desiredSchedule?.id  != it.id
        }
        def friendsInCourses = [:]
        
        log.info "Mapping friends and courses"
        for (section in sections) {
                
                def friends = friendsSchedules?.findAll{
                        section in it?.courses
                }.collect{
                        it.student
                }
                log.info  "test" + section
                friendsInCourses[section] = friends.collect { it.firstName + " " + it.lastName}
        }
        
		render view:'desired',model:[schedule:user.desiredSchedule,courses:sections,friendsInCourses:friendsInCourses]
	}

	/**
	 * Action to display the create desired schedule form
	 * If the student already has a saved criteria, auto fill the form
	 *
	 * @return the create desired schedule form, auto filled if criteria saved
	 */
	def createDesiredSchedule(){
		try{
			def user = (Student)springSecurityService.currentUser;
			def criteria=user.desiredScheduleCriteria
			if(!criteria)criteria=new DesiredScheduleCriteria()
			render view:'createDesiredSchedule',model:[desiredScheduleCriteriaInstance:criteria]
		}
		catch(Exception e){
			redirect action:'desired'
		}
	}


	/**
	 * Action to save criteria to database and attempt to generate desired schedule
	 * If the schedule could be generated, redirect to desired and display it
	 * Otherwise, redirect to desired and display the suggested changes
	 *
	 * @return the generated schedule or the suggested changes
	 */
	@Transactional
	def generateDesired() {
		log.info "generateDesired entered"
		try{
			//Save criteria to database
			log.info "parsing desired schedule criteria form"
			def user = (Student)springSecurityService.currentUser;
			if(!params.year.isInteger())
				throw new Exception("Year is invalid")
			if(!params.semester.isInteger())
				throw new Exception("Semester is invalid")
			DesiredScheduleCriteria dsc=user.desiredScheduleCriteria
			if(!dsc)dsc=new DesiredScheduleCriteria()
			else{
				user.desiredScheduleCriteria=null
				dsc.delete()
				dsc=new DesiredScheduleCriteria()
			}
			dsc.year=params.year as int
			dsc.semester=params.semester as int
			dsc.student=user
			dsc.lunchDesiredDays=params.lunchDesiredDays?.trim()
			def courseIds=params.list('desiredCourseIdentity')
			def preferredTeachers=params.list('desiredCourseTeacher')
			//dsc.courses?.clear()//add desired courses
			for(int i=0;i<courseIds.size();i++){
				def cid=courseIds[i].trim()
				def teacher=preferredTeachers[i].trim()
				def school=user.school
				def c=Course.findWhere(identity:cid,school:school)
				def course=new DesiredScheduleCourse(course:c,teacher:teacher)
				dsc.addToCourses(course)
			}
			//dsc.excludedSections?.clear()//add sections to exclude
			def excludedCourseIds=params.list('excludedCourseIds')
			for(cs in excludedCourseIds){
				CourseSection s=CourseSection.get(cs as long)
				dsc.addToExcludedSections(s)
			}
			user.desiredScheduleCriteria=dsc
			log.info "Criteria saved to database"
			//attempt to generate schedule
			log.info "Attempting to generate schedule"
			DesiredScheduleFactory f=new DesiredScheduleFactory()
			def genSchedule=f.createDesiredSchedule(dsc)
			if(user.desiredSchedule){
				def oldSchedule=user.desiredSchedule
				oldSchedule.courses.clear()
				oldSchedule.save()
				user.desiredSchedule=null
				oldSchedule.delete()
			}
			user.desiredSchedule=genSchedule
			user.save(flush:true)
			if(genSchedule){
				log.info "schedule genereated, redirecting to desired"
				flash.message="Generated schedule"
			}
			else{
				log.info "schedule not generated, redirecting to desired"
				flash.changes=f.scheduleSuggestedChanges.entrySet()
			}
			redirect action:'desired'
		}
		catch(Exception e){
			log.error "Exception occurred"
			log.error e.stackTrace.toString()
			flash.error=e.getMessage()
			redirect action:'createDesiredSchedule'
		}
	}

	/**
	 * Exports the desired schedule into a temporary .txt file
	 * <p>
	 * The course sections are sorted by sortSection. Each section is by the following:
	 * <ul>
	 * <li> #.
	 * <li> Course: id
	 * <li> Teacher: name
	 * <li> Periods: periods
	 * <li> Days: days
	 * <ul>
	 * Where # is an incrementing number from 1 to the number of sections in the schedule,
	 * id is the identifier of the course,
	 * name is the name of the teacher,
	 * periods is the comma-separated list of periods, and
	 * days is the comma-separated list of days
	 * <p>
	 *
	 * @param
	 * @return
	 */
	def exportDesired() {
		def user = (Student)springSecurityService.currentUser;
		def sections=[]
		if(user.desiredSchedule){
			boolean withFriends=false
			if(params.showHideFriends){
				withFriends=true
			}
			log.info "found desired schedule"
			Schedule s=user.desiredSchedule
			sections = s?.courses?.toArray()
			sections = sortSections(sections)
			
			def friendsInCourses = [:]
			if (withFriends) {
				log.info "Getting user's friends in the same school"
				def usersFriends = user?.friends?.toArray().findAll{
					it.school == user.school
				}

				log.info "Getting friends' schedules from the same semester"
				def friendsSchedulesList = usersFriends?.assignedSchedules?.flatten()
				def friendsSchedules = friendsSchedulesList?.toArray()?.findAll{
					it.year == s.year && it.semester == s.semester && it.student?.desiredSchedule?.id  != it.id
				}

				log.info "Mapping friends and courses"
				for (section in sections) {
					def friends = friendsSchedules?.findAll{
						section in it.courses
					}.collect{
						it.student
					}
					friendsInCourses[section] = friends.collect { it.firstName + " " + it.lastName}
				}
			}
			
			log.info "exportDesired entered"
			File file = File.createTempFile("desired",".txt")
			String out = ""
			int count=1
			for(CourseSection c in sections)
			{
				out = out + count + ".\r\n"
				out = out + "Course: " + c.course.identity + "\r\n"
				out = out + "Teacher: " + c.teacher + "\r\n"
				out = out + "Periods: " + c.block.periods + "\r\n"
				out = out + "Days: " + c.block.days + "\r\n"
				if(withFriends)
					out = out + "Friends: " + friendsInCourses[c]?.join(",") + "\r\n"
				count++
			}
			file.write(out)

			response.setHeader "Content-disposition", "attachment; filename=${file.name}"
			response.contentType = 'text-plain'
			response.outputStream << file.text
			response.outputStream.flush()
			response.outputStream.close()

			//redirect action: "desired"
		}
		else{
			flash.error="No desired schedule generated"
			redirect action:'desired'
		}
	}

	/**
	 * Action to view all course sections for a user's school
	 *
	 * @param order the order to sort course section (asc or desc)
	 * @return a sorted ordering of all course sections offered by the user's school
	 */
	def courses() {
		log.info "courses entered"

		def user = (Student)springSecurityService.currentUser;
		int currentYear=Calendar.getInstance().get(Calendar.YEAR)
		int year=currentYear
		try{
			if(params.year)
				year=params.year as int
		}
		catch(Exception e){
			year=currentYear
		}
		def school = (School)user.school;
		def courses = Course.findAllBySchool(school)
		def students = Student.findAllBySchool(school)

		println students.assignedSchedules.flatten()

		def courseSections = []
		def studentsInSections = [:]
		for(c in courses)
		{
			CourseSection.findAll {
				course==c&&block.schoolAcademicYear.year==year
			}.each{
				courseSections.add(it)
				studentsInSections[it] = students.assignedSchedules?.flatten()?.count{ schedule ->
					it in schedule.courses && schedule.id != schedule.student.desiredSchedule?.id
				}
				println studentsInSections[it]
			}
		}

		def courseCount = Course.count();
		render view:'courses', model:[courseSectionsInstanceList:courseSections, courseInstanceList:courses,
			 courseInstanceCount:courseCount, studentsCount:studentsInSections,minYear:MIN_YEAR,maxYear:currentYear+2,currentYear:year ]
	}

	/**
	 * Returns a string of <option>s for each section associated with a course identifier
	 * Each option displays the identifier, teacher, periods, and days
	 *
	 * @param identity the course identifier
	 * @return a string of <option>s for each related section
	 */
	def searchByCourseIdentifier(String identity){
		log.info "searchByCourseIdentifier entered"
		try{
			identity=identity?.trim()
			def user = (Student)springSecurityService.currentUser;
			StringBuilder sb=new StringBuilder()
			log.info "searching for course id: ${identity}"
			Course course=Course.findWhere(identity:identity,school:user.school)
			if(!course)
			{
				render""
				return
			}
			log.info "course found"
			def year = params.year as int
			def semester = params.semester as int
			for(cs in course.sections){
				if(cs.block.schoolAcademicYear.year == year &&
					cs.semesterStart <= semester && cs.semesterEnd >= semester)
					sb.append("<option value='${cs.id}'>${cs.course.identity} - ${cs.teacher} - Period:${cs.block.periods} - Days:${cs.block.days}</option>")
			}
			render sb.toString()
		}
		catch(NumberFormatException e){
			log.info "NumberFormatException occurred"
			log.error e.stackTrace.toString()
			response.status = 500
			render "Year and semester are invalid"
		}
		catch(Exception e){
			log.error "Exception occurred"
			log.error e.stackTrace.toString()
			response.status = 500
			render e.getMessage()
		}
	}

	/**
	 * Appends a given course section to the list of excluded course sections
	 * in the create desired schedule form
	 *
	 * @param id the course section id
	 * @return render a row for the excluded sections part of the create schedule form
	 */
	def addToExcludedCourseSections(){
		try{
			log.info "addToExcludedCourseSections entered"
			def section=CourseSection.get(params.id as long)
			log.info "section retrieved"
			render template:'/schedule/excluded_course_section_row',model:[section:section]
		}
		catch(Exception e){
			log.error "Exception occurred"
			log.error e.stackTrace.toString()
			response.status = 500
			render e.getMessage()
		}
	}

	/**
	 * Create a new DesiredScheduleCourse from a course identifier and display a row
	 *
	 * @param identity the course identifier
	 * @return render a row for the desired courses part of the create schedule form
	 */
	def addToDesiredCourses(String identity){
		log.info "addToDesiredCourses entered"
		try{
			identity=identity?.trim()
			def user = (Student)springSecurityService.currentUser;
			log.info "searching for course id: ${identity}"
			Course course=Course.findWhere(identity:identity,school:user.school)
			if(!course)throw new Exception("Course not found")
			DesiredScheduleCourse d=new DesiredScheduleCourse(course:course,teacher:"")
			render template:'/schedule/desired_courses_row',model:[desiredScheduleCourseInstance:d]
		}
		catch(Exception e){
			log.error "Exception occurred"
			log.error e.stackTrace.toString()
			response.status = 500
			render e.getMessage()
		}
	}

	def desiredSchedule(String username) {
		log.info "getting $username's desired schedule"
		try {
			def user = (Student)springSecurityService.currentUser
			def friend = Student.findByUsername(username)

			def isFriend = user?.friends?.find{it.username == username} != null
			if (!isFriend) {
				throw new Exception("You do not have access to view this schedule!")
			}

			if (friend.desiredSchedule == null)
				throw new Exception("Desired schedule could not found.")

			renderSchedule(friend.desiredSchedule)
		}
		catch(Exception e){
			log.error "Exception occurred"
			log.error e.stackTrace.toString()
			render e.getMessage()
		}
	}

	def assignedSchedule(String username, int year, int semester) {
		log.info "getting $username's assigned schedule for $year, $semester"
		try {
			def user = (Student)springSecurityService.currentUser
			def friend = Student.findByUsername(username)

			def isFriend = user?.friends?.find{it.username == username} != null
			if (!isFriend) {
				throw new Exception("You do not have access to view this schedule!")
			}

			Schedule schedule = friend.assignedSchedules?.find{
				it.year == year && it.semester == semester && it.id != friend.desiredSchedule?.id
			}

			if (!schedule)
				throw new Exception("Assigned schedule could not found.")

			renderSchedule(schedule)
		}
		catch(Exception e){
			log.error "Exception occurred"
			log.error e.stackTrace.toString()
			render e.getMessage()
		}
	}

	private def renderSchedule(Schedule schedule) {
		log.info "rendering schedule $schedule"

		def sections = sortSections(schedule?.courses?.toArray())

		render template:'readonly_schedule', model:[sections:sections]
		
	}
}
