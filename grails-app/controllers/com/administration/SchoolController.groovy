package com.administration

import static org.springframework.http.HttpStatus.*

import com.schedule.Schedule;
import com.student.Student;

import grails.transaction.Transactional

@Transactional(readOnly = true)
class SchoolController {

	static allowedMethods = [save: "POST", update: "PUT"]

	def index(Integer max) {
		log.info "index Entered";
		params.max = Math.min(max ?: 10, 100)
		if(params.sort == null){
			params.sort = "name"
			params.order = "asc"
		}
		respond School.list(params), model:[schoolInstanceCount: School.count()]
	}

	def show(School schoolInstance) {
		respond schoolInstance
	}

	def create() {
		respond new School(params)
	}

	/**
	 * Action to save the add school form to database
	 * @return
	 */
	@Transactional
	def save() {
		log.info "save entered"
		School school
		try{
			def errors = []
			school = new School()
			//Get request parameters
			String name = params.name?.trim()
			String numSemesters = params.numSemesters?.trim()
			String numDays = params.numDays?.trim()
			String numPeriods = params.numPeriods?.trim()
			String lunchPeriodStart = params.lunchPeriodStart?.trim()
			String lunchPeriodEnd = params.lunchPeriodEnd?.trim()
			def years = params.list('years')
			def blocks = params.list('scheduleBlocks')
			//validate inputs
			if(name.isEmpty())
				errors.add("Name must be entered")
			if(School.findWhere(name:name))
				errors.add("School exists with name: ${name}")
			school.name = name
			if(numSemesters.isEmpty())
				errors.add("Semesters must be entered")
			school.numSemesters = Integer.parseInt(numSemesters)
			if(numDays.isEmpty())
				errors.add("Days must be entered")
			school.numDays = Integer.parseInt(numDays)
			if(numPeriods.isEmpty())
				errors.add("Periods must be entered")
			school.numPeriods = Integer.parseInt(numPeriods)
			if(lunchPeriodStart.isEmpty())
				errors.add("Lunch period start must be entered")
			school.lunchPeriodStart = Integer.parseInt(lunchPeriodStart)
			if(lunchPeriodEnd.isEmpty())
				errors.add("Lunch period end must be entered")
			school.lunchPeriodEnd = Integer.parseInt(lunchPeriodEnd)
			//validate school period logic
			if(school.lunchPeriodEnd > school.numPeriods)
				errors.add("Lunch period end cannot be greater than periods")
			if(school.lunchPeriodStart > school.numPeriods)
				errors.add("Lunch period start cannot be greater than periods")
			if(school.lunchPeriodEnd < school.lunchPeriodStart)
				errors.add("Lunch period start cannot be greater than lunch period end")

			if(blocks?.isEmpty())//check if school block has been entered
				errors.add("A year of schedule blocks must be entered")
			else{
				try{
					for(int i = 0; i < years.size(); i++){//loop over each academic year entered
						String year = years[i]?.trim()
						if(!year.isEmpty()){
							AcademicYearScheduleBlockSet yearBlocks = new AcademicYearScheduleBlockSet(year:(year as int), school:school)
							def blockList = ScheduleBlock.parseScheduleBlockString(blocks[i])//parse schedule block string into list of ScheduleBlocks
							log.info "parsing schedule block"
							for(b in blockList){//add blocks to school
								log.info "validating schedule block"
								if(school.numPeriods < (b.periods as int)){
									log.info "Invalid period in schedule block found ${b}"
									throw new Exception()
								}
								for(d in b.days.split(",")){
									if(school.numDays < (d as int))
									{
										log.info "Invalid days in schedule block found ${b}"
										throw new Exception()
									}
								}
								yearBlocks.addToScheduleBlocks(b)
							}
							school.addToAcademicYearSchedules(yearBlocks)
							log.info "academic year added"
						}
					}
					log.info "finished added schedule blocks"
				}
				catch(Exception e){
					errors.add("Invalid schedule block(s)")
				}
			}
			if (!errors.isEmpty()) {//check if validation errors have occurred
				log.info "validation errors occurred"
				flash.errors = errors
				render view:'create',model:[schoolInstance:school]
				return
			}
			school.save(flush:true)//save school
			log.info "school saved"
			redirect action:'index'
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			flash.error = e.getMessage()
			render view:'create',model:[schoolInstance:school]
		}
	}

	def edit(School schoolInstance) {
		log.info "edit entered"
		render view:"edit",model:[schoolInstance:schoolInstance]
	}

	/**
	 * Action to handle updating a school's information
	 * @param schoolInstance the school to update
	 * @return
	 */
	@Transactional
	def update(School schoolInstance) {
		log.info "update entered"
		try{
			if (schoolInstance == null)
				throw new Exception("Could not locate school")
			def years = params.list('years')
			def scheduleBlocks = params.list('scheduleBlocks')
			log.info "retrieve academic year parameters"
			def currentYears=schoolInstance.academicYearSchedules
			def currentYearsMap=[:]
			for(y in currentYears)
				currentYearsMap.put(y.year,y)
				log.info "looping over all years submitted"
			for(int i = 0; i < years.size(); i++){
				String yearString = years[i].trim()
				if(!yearString.isEmpty()){
					int year=yearString as int
					log.info "parsing schedule block string"
					def blockSet = ScheduleBlock.parseScheduleBlockString(scheduleBlocks[i])
					AcademicYearScheduleBlockSet academicYear
					if(currentYearsMap.containsKey(year)){
						log.info "academic year already exists for school"
						academicYear=currentYearsMap.get(year)
						currentYearsMap.remove(year)
						def currentScheduleBlocks=[:]
						for(b in academicYear.scheduleBlocks.toArray())//create map to see if block has been deleted
							currentScheduleBlocks.put(b, false)
						for(b in blockSet){
							b.schoolAcademicYear=academicYear
							boolean exists=false
							for(c in currentScheduleBlocks){
								if(c.key.equals(b)){//check if schedule block already exists
									log.info "schedule block ${b} already exists"
									exists=true
									c.value=true
									break
								}
							}
							if(!exists){
								log.info "schedule block ${b} added to academic year"
								academicYear.addToScheduleBlocks(b)
							}
						}
						log.info "removing old schedule blocks"
						for(b in currentScheduleBlocks){
							if(!b.value){
								//remove course sections
								def schedules=Schedule.createCriteria().list {
									and{
										courses{
											eq('block',b.key)
										}
									}
								}
								log.info "removing course sections from schedules"
								for(s in schedules){
									for(cs in b.key.courseSections)
										s.removeFromCourses(cs)
									s.save(flush:true)
								}
								log.info "removing schedule block from academic year"
								academicYear.removeFromScheduleBlocks(b.key)
								b.key.delete(flush:true)
								log.info "schedule block removed"
							}
						}
					}
					else{
						log.info "academic year does not exist for school"
						academicYear = new AcademicYearScheduleBlockSet(school:schoolInstance, year:year)
						log.info "add schedule blocks to new academic year"
						for(b in blockSet){
							academicYear.addToScheduleBlocks(b)
						}
						schoolInstance.addToAcademicYearSchedules(academicYear)
						log.info "academic year added to school"
					}
				}
			}
			log.info "removing old academic years from school"
			for(y in currentYearsMap){//remove years no longer used in school
				def schedules=Schedule.createCriteria().list {
					and{
						eq('year',y.value.year)
						student{
							eq('school',schoolInstance)
						}
					}
				}
				log.info "removing all schedules in the academic year"
				for(s in schedules){
					s.courses.clear()
					s.delete(flush:true)
				}				
				schoolInstance.removeFromAcademicYearSchedules(y.value)
				y.value.delete(flush:true)
				log.info "academic year removed from school"
			}
			schoolInstance.save(flush:true)
			log.info "school update complete"
			flash.message="${schoolInstance.name} updated"
			redirect action:'edit',id:schoolInstance.id
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			flash.error = e.getMessage()
			redirect action:'index'
		}
	}

	/**
	 * Action to delete a school from system
	 * @param schoolInstance the school to delete
	 * @return
	 */
	@Transactional
	def delete(School schoolInstance) {
		log.info "delete entered"
		def students = Student.findAllWhere(school:schoolInstance)
		for(s in students){//remove each student from school and remove schedule containing courses from database
			def assignedSchedules = s.assignedSchedules.toArray()
			for(sch in assignedSchedules){
				sch.courses.clear()
				sch.save(flush:true)
				s.removeFromAssignedSchedules(sch)
				sch.delete(flush:true)
			}
			s.assignedSchedules = null
			s.desiredSchedule = null
			s.school = null
			s.save(flush:true)
		}
		log.info "remove students from school and schedules"
		if (schoolInstance == null) {
			notFound()
			return
		}

		schoolInstance.delete flush:true
		log.info "school deleted"

		redirect action:'index'
	}

	protected void notFound() {
		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.not.found.message', args: [
					message(code: 'school.label', default: 'School'),
					params.id
				])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}
}
