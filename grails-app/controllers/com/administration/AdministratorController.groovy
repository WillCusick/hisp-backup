package com.administration

import static org.springframework.http.HttpStatus.*

import com.security.AccountRequest;
import com.security.SecRole;
import com.security.User
import com.security.UserSecRole;
import com.student.Student;

import grails.transaction.Transactional

@Transactional(readOnly = true)
class AdministratorController {

	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	def grailsApplication
	def studentService

	/**
	 * Action to generate and render the administrator dashboard
	 * @return
	 */
	def index() {
		log.info "index entered"

		def accountRequestCount = AccountRequest.count();//get the number of requests
		render view:'index', model:[accountRequestCount:accountRequestCount];
	}
	/**
	 * Action to show student account requests
	 * @return
	 */
	def requests(){
		log.info "requests entered"

		def sortOrder = ""
		if (params.order == "asc" || params.order == "desc")//check to user selected sort order
			sortOrder = params.order
		else
			sortOrder = "asc"

		// Using criteria instead of AccountRequest.list(sort: ..., order: ...)
		// is required because of some problems with GORM and nested properties
		def criteria = AccountRequest.createCriteria()
		def requests
		switch (params.sort) {//get list of requests sorted by sort column
			case "student.school.name":
				requests = criteria.list {
					student {
						school { order("name", sortOrder) }
					}
				}
				break;
			case "student.firstName":
				requests = criteria.list {
					student { order("firstName", sortOrder) }
				}
				break;
			case "student.lastName":
				requests = criteria.list {
					student { order("lastName", sortOrder) }
				}
				break;
			case "student.username":
				requests = criteria.list {
					student { order("username", sortOrder) }
				}
				break;
			case "student.email":
				requests = criteria.list {
					student { order("email", sortOrder) }
				}
				break;
			case "dateTime":
			default:
				requests = criteria.list { order("dateTime", sortOrder) }
		}

		def requestCount = AccountRequest.count()
		render view:'requests', model:[accountRequestInstanceList:requests,accountRequestInstanceCount:requestCount]
	}
	/**
	 * Action to reject an account request
	 * @param r the account request to reject
	 * @return
	 */
	@Transactional
	def rejectRequest(AccountRequest r){
		log.info "rejectRequest entered"

		try{
			if (!r)
				throw new Exception("Account request could not be located")
			def user = r.student//get request's user
			if (r.delete())//remove request from database
				log.info "Deleted request"
			if (user.delete())//remove user from database
				log.info "Deleted user"
			flash.message="Account request rejected"
			redirect action:'requests'
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			flash.error = e.getMessage()
			redirect action:'requests'
		}
	}
	/**
	 * Action to approve account requests
	 * @param r the account request to approve
	 * @return
	 */
	@Transactional
	def approveRequest(AccountRequest r){
		log.info "approveRequest entered"

		try{
			approveAccountRequest(r)//approve request
			flash.message = "Account request approved"
			redirect action:'requests';
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			flash.error = e.message
			redirect action:'requests'
		}
	}
	/**
	 * Action to approve all pending account requests
	 * @return
	 */
	@Transactional
	def approveAllRequests(){
		log.info "approveAllRequests entered"

		try{
			def requests=AccountRequest.findAll()//find all requests in database
			for (r in requests) {
				approveAccountRequest(r, true)
			}
			flash.message = "All account requests approved"
			redirect action:'requests';
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			flash.error = e.message
			redirect action:'requests';
		}
	}

	/**
	 * Method to approve an account request
	 * @param r account request to approve
	 * @return
	 * @throws Exception
	 */
	private def approveAccountRequest(AccountRequest r, boolean flushImmediately = false) throws Exception{
		if (!r)
			throw new Exception("Account request could not be located")

		// Enable student
		Student s = r.student
		s.enabled = true
		s.save(flush:flushImmediately)

		// Add authentication for the student
		def studentRole = SecRole.findWhere(authority: 'ROLE_STUDENT')
		if (UserSecRole.create(s, studentRole, true))
			log.info "User account approved"

		// Delete request
		if (r.delete(flush:flushImmediately))
			log.info "Request removed"

		if (!grailsApplication.config.grails.mail.disabled)
			notifyByEmail(s.email, s.firstName, s.lastName)
	}

	/**
	 * Notify user by email of account approval
	 * @param email email address to send message to
	 * @param firstName user's first name
	 * @param lastName user's last name
	 * @return
	 */
	private def notifyByEmail(String email, String firstName, String lastName){
		sendMail {
			async true
			to email
			subject "HISP Account Approved"
			html """<h2>Welcome to HISP</h2>
				<p>${firstName} ${lastName}, your student account request at HISP has been approved.<p>
				<p>You can now login and enjoy everything our site has to offer!<p>"""
		}
	}
	/**
	 * Action to view all student accounts in the system
	 * @return
	 */
	def studentAccounts(){
		log.info "studentAccounts entered"

		def max = Math.min(params.max?.toInteger() ?: 10, 100)
		def offset = params.offset ? params.offset.toInteger() : 0
		def students = Student.findAllByEnabled(true, [offset: offset, max: max])

		render view:'studentAccounts', model:[studentInstanceList:students, studentInstanceCount: students.size()]
	}
	/**
	 * Action to delete a student account from the system.
	 * @param s the student account to be deleted
	 * @return
	 */
	@Transactional
	def deleteStudent(Student s){
		log.info "deleteStudent entered"

		try{
			studentService.deleteStudent(s)
			/*if(!s)
				throw new Exception("Student account could not be located")
			if(!s.enabled)
				throw new Exception("Student account not enabled")
			Collection<UserSecRole> roles = UserSecRole.findAllByUser((User)s);//find all security roles for student account
			roles*.delete(flush:true)
			for(f in s.friends)
				f.removeFromFriends(s)
			if (s.delete())//delete student account from database
				log.info "Student deleted"*/
			flash.message = "Student account deleted"
			redirect action:'studentAccounts'
		}
		catch(Exception e){
			log.info "Exception occurred"
			log.error e.stackTrace.toString()
			flash.error = e.message
			redirect action:'studentAccounts'
		}
	}
}
