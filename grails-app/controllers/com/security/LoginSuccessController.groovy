package com.security

import com.student.Student;

class LoginSuccessController {
	def springSecurityService
	/**
	 * Action to redirect to the student or administrator dashboard depending 
	 * on what type of user logged in.
	 * @return
	 */
	def index(){
		if (springSecurityService.isLoggedIn()) {
			log.info "Login successful - redirecting"
			
			// Get current user from spring security service
			def user = springSecurityService.getCurrentUser()

			// Check if username exists in Student table
			if(Student.findWhere(username:user.username)) {
				log.info "Redirect student"
				redirect controller:'student'
			} else {
				log.info "Redirect admin"
				redirect controller:'administrator'
			}
		}
	}
}
