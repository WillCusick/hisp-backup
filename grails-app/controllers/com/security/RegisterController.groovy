package com.security

import com.administration.School;
import com.student.Student;

class RegisterController extends grails.plugin.springsecurity.ui.RegisterController {
	/**
	 * Action to show register page
	 */
	def index(){
		render view:'index',model:[schools:getSchoolList()]
	}
	/**
	 * Action to register user and create account request for approval
	 * @return
	 */
	def register(){
		def errorList = []
		try{
			String username = params.username?.trim()
			if(!username || username?.size()<4)
				errorList.add("Username must be at least 4 characters")
			else if(User.findWhere(username:username))
				errorList.add("Username already exists");
			
			String email = params.email?.trim()
			if(!email || !validEmail(email))
				errorList.add("Invalid email")
				
			String password = params.password
			if(!password || password.size()<4)
				errorList.add("Password must be at least 4 characters")
				
			String password2 = params.password2
			if(!password?.equals(password2))
				errorList.add("Passwords do not match")
				
			String firstName = params.firstName?.trim()
			if(!firstName || firstName.isEmpty())
				errorList.add("First name must be entered")
				
			String lastName = params.lastName?.trim()
			if(!lastName || lastName.isEmpty())
				errorList.add("Last name must be at entered")
				
			String schoolId = params.school;
			if(schoolId as long == -1)
				errorList.add("School must be selected")
				
			if(!errorList.isEmpty())
				throw new Exception("Error Occurred")

			// Create disabled user account in database to be enabled
			// when account request is approved by administrator
			Student s = new Student(enabled:false, email:email, firstName:firstName,
				lastName:lastName, username:username, password:password,
				school:School.get(schoolId as long))
			s.save()

			//Create account request for student in database
			AccountRequest req = new AccountRequest(student:s, dateTime:new Date());
			req.save();
			render view:"index", model:[success:true];
		} catch(Exception e){
			if(errorList.isEmpty())
				errorList=[e.message]
			render view:'index', model:[formValues:params,schools:getSchoolList(),
				errors:errorList]
		}
	}

	/**
	 * Method to validate email addresses. Emails are valid if they have some
	 * number of characters, an '@' symbol, followed by some number of characters
	 * @param e email to validate
	 * @return true if email is valid, false otherwise
	 */
	private boolean validEmail(String e){
		return e ==~ /.+@.+/
	}

	/**
	 * Get list of schools sorted by name
	 * @return
	 */
	private ArrayList<School> getSchoolList(){
		return School.list(sort:'name')
	}
}
