package com.student

import com.administration.School
import com.schedule.DesiredScheduleCriteria;
import com.schedule.Schedule;
import com.security.User;

class Student extends User{
	String firstName
	String lastName
	String email
	School school
	Schedule desiredSchedule
	DesiredScheduleCriteria desiredScheduleCriteria
	
	static mapping={
		version false
		friends joinTable:[name: 'STUDENT_FRIENDS']
		friendRequests joinTable:[name: 'STUDENT_FRIEND_REQUESTS']
		firstName defaultValue: ""
		lastName defaultValue:""
		assignedSchedules cascade:'all'	
	}
    static constraints = {
		firstName nullable:false
		lastName nullable:false
		email nullable:false,unique:true
		school nullable:true
		desiredSchedule nullable:true
		desiredScheduleCriteria nullable:true
    }
	static hasMany=[friends:Student,friendRequests:Student,assignedSchedules:Schedule]
}
