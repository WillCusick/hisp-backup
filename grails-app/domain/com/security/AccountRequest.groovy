package com.security

import com.student.Student

class AccountRequest {

	Student student
	Date dateTime
	
    static constraints = {
		student unique:true,nullable:false
		dateTime nullable:false
    }
	static mapping={
		version(false);
	}
}
