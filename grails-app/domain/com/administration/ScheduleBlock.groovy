package com.administration

import java.util.ArrayList;

import com.schedule.CourseSection;

class ScheduleBlock {
	String periods;
	String days;
	static constraints = {
	}
	static mapping = { courseSections cascade:'all' }
	static belongsTo = [schoolAcademicYear:AcademicYearScheduleBlockSet]
	static hasMany=[courseSections:CourseSection]

	/**
	 * Method that parses a schedule block input string into a list of ScheduleBlocks
	 * @param b string to be parsed
	 * @return list of schedule blocks
	 */
	static ArrayList<ScheduleBlock> parseScheduleBlockString(String b){
		def elements=b.split(">")
		def blockList=[]
		for(e in elements){//loop over each element
			e=e.substring(1)//remove front <
			def periodDayArray=e.split(';')
			def periods = periodDayArray[0].split(',')
			for (p in periods)
				Integer.parseInt(p)
			def days=periodDayArray[1].split(',')
			for(d in days)
				Integer.parseInt(d)
			ScheduleBlock block=new ScheduleBlock();
			block.periods=periodDayArray[0]
			block.days=periodDayArray[1]
			blockList.add(block)
		}
		return blockList
	}
	public String toString(){
		return "periods: ${periods}, days: ${days}"
	}
	public boolean equals(ScheduleBlock b){
		if(periods!=b.periods)return false
		if(days!=b.days)return false
		if(schoolAcademicYear.year!=b.schoolAcademicYear.year)return false
		return true
	}
}
