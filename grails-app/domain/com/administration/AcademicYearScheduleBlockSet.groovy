package com.administration

class AcademicYearScheduleBlockSet {
	Integer year
	static belongsTo=[school:School]
	static hasMany=[scheduleBlocks:ScheduleBlock]
    static constraints = {
		year(unique: ['school'])
    }
	static mapping={
		scheduleBlocks sort:'periods', cascade:'all'
	}
	public String getScheduleBlocksString(){
		StringBuilder sb=new StringBuilder()
		for(s in scheduleBlocks){
			sb.append("<${s.periods};${s.days}>")
		}
		return sb.toString()
	} 
}
