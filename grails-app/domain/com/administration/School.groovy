package com.administration

import com.schedule.Course;

class School {
	String name
	Integer numSemesters
	Integer numDays
	Integer numPeriods
	Integer lunchPeriodStart
	Integer lunchPeriodEnd
	
    static constraints = {
		name nullable:false, unique:true
		numSemesters nullable:false, range:1..4
		numDays nullable:false, range:1..7
		numPeriods nullable:false, range:1..12
		lunchPeriodStart nullable:false, validator:{val, obj -> val >= 1 && val <= obj.lunchPeriodStart}
		lunchPeriodEnd nullable:false, validator:{val, obj -> val >= obj.lunchPeriodEnd && val <= obj.numPeriods}
    }
	static mapping={
		academicYearSchedules sort:'year',cascade:'all'
		courses cascade:'all'
	}
	static hasMany=[academicYearSchedules:AcademicYearScheduleBlockSet,courses:Course]
	public String toString(){
		return name;
	}
}
