package com.schedule

import com.administration.ScheduleBlock;
import com.student.Student;

class CourseSection {
	String teacher
	Integer semesterStart
	Integer semesterEnd
	static belongsTo=[block:ScheduleBlock,course:Course]
	static constraints = {
		block(unique: ['course', 'semesterStart', 'semesterEnd'])
		semesterStart validator:{val, obj -> val >= 1 && 
			val <= obj.block.schoolAcademicYear.school.numSemesters &&
			val <= obj.semesterEnd}
		semesterEnd validator:{val, obj -> val >= obj.semesterStart && val <= obj.block.schoolAcademicYear.school.numSemesters}
	}
	static mapping = {
	}
	public String toString(){
		return "${course.identity} - ${teacher} (${block})"
	}
}
