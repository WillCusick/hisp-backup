package com.schedule

import com.administration.School;

class Course {
	String identity
	String title
    static constraints = {
		identity(unique: ['school'])
	}
	static mapping = {
		sections cascade:'all'
	}
	static hasMany = [sections:CourseSection]
	static belongsTo = [school:School]
	public String toString(){
		return "${identity}"
	}
}