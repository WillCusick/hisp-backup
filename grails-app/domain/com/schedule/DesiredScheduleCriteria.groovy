package com.schedule

import com.student.Student;

class DesiredScheduleCriteria {
	Integer year
	Integer semester
	String lunchDesiredDays
	static hasMany=[courses:DesiredScheduleCourse,excludedSections:CourseSection]
	static belongsTo=[student:Student]
    static constraints = {
    }
	static mapping={
		excludedSections joinTable:[name: 'DESIRED_SCHEDULE_CRITERIA_EXCLUDED_SECTIONS']
	}
}
