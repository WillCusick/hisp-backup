package com.schedule

import com.student.Student;

class Schedule {
	Integer year
	Integer semester
	Date lastUpdated
    static constraints = {
    }
    static mapping = {
    	courses cascade:'all'
    }
	static hasMany = [courses:CourseSection]
	static belongsTo = [student:Student]
}
