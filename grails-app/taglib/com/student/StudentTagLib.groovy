package com.student

class StudentTagLib {
	def springSecurityService
	def studentSiderbar={attrs, body ->
		String controller=controllerName;
		String action=actionName;
		def items=[:];
		switch(controller){
			case 'friends':
				items=[
					'My Friends':['friends', 'myFriends'],
					'Add Friends':['friends', 'manageFriends'],
					'Friend Requests':['friends', 'requests']]
				break;
			case 'schedule':
				items=[
					'My Courses':['schedule', 'assigned'],
					'View Courses':['schedule', 'courses'],
					'Desired Schedule':['schedule', 'desired']]
				break;
			case 'student':
				items=['Dashboard':['student', 'index'],'My Account':['student', 'myAccount']]
				break;
		}
		out<<render(template:'/student/student_sidebar',model:[itemMap:items])
	}
	def currentUsername={attrs,body->
		out<<springSecurityService.currentUser.username
	}
	def currentName={attrs,body->
		def user = springSecurityService.currentUser
		out<<user.firstName + " " + user.lastName
	}
}
