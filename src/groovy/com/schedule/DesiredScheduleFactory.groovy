package com.schedule

import com.administration.ScheduleBlock;
import com.schedule.Schedule;
import com.student.Student;

class DesiredScheduleFactory {
	private static final int TEACHER_MATCH=10
	private static final int STUDENT_FRIEND_MATCH=1
	private static final int MAX_CHANGE_SUGGESTIONS=2
	private ArrayList<CourseSection> excludedSections
	private ArrayList<Schedule> friendsSchedules
	private ArrayList<DesiredScheduleCourse> desiredCourses
	private DesiredScheduleCriteria criteria
	private Map<ArrayList<CourseSection>,ArrayList<String>> scheduleSuggestedChanges
	private ArrayList<Node> attemptedNodes
	public Schedule createDesiredSchedule(DesiredScheduleCriteria criteria){
		def user=criteria.student
		friendsSchedules = criteria.student?.friends?.toArray()?.findAll{
			it.school == user.school
		}?.assignedSchedules?.flatten()?.toArray()?.findAll{
			it.year == criteria.year && it.semester == criteria.semester &&
					it.student?.desiredSchedule?.id != it.id
		}
		def possibleSections=[]
		this.criteria=criteria
		desiredCourses=criteria.courses.toList()
		excludedSections=criteria.excludedSections
		scheduleSuggestedChanges=new HashMap()
		attemptedNodes=new ArrayList()
		Node n =createTree()
		printBFS(n)
		return createSchedule(n)
	}
	private Node createTree(){
		Node head=new Node()
		//Loop through each desired course and add it to scheduling tree
		for(int i=0;i<desiredCourses.size();i++){
			addToTree(desiredCourses[i].course, head)
		}
		return head
	}
	private void addToTree(Course c,Node node){
		if(!node.nextNodes.isEmpty()){//check if node is a leaf
			for(n in node.nextNodes){//not leaf pass course down to each child of node
				addToTree(c, n)
			}
		}
		else{//node is leaf
			def sections=c.sections.findAll{
				it.block.schoolAcademicYear.year==criteria.year
			}.toArray()
			for(s in sections){//add new child nodes for each sections of course
				if (criteria.semester >= s.semesterStart  && criteria.semester <= s.semesterEnd) {
					// Only add section if the semester range is appropriate
					Node newNode=new Node(parent:node,section:s)
					if(!conflict(newNode)&&!excludedSection(s)){//check if section conflicts with any parent node
						newNode.score=calculateNodeScore(newNode)
						node.nextNodes.add(newNode)
					}
				}
			}
		}
	}
	private boolean excludedSection(CourseSection section){
		for(s in excludedSections){
			if(s.id==section.id)
				return true
		}
		return false
	}
	private boolean conflict(Node n){
		def days=n.section.block.days.split(',')
		def nPeriods=n.section.block.periods.split(",")
		Node parent=n.parent
		while(parent!=null){
			if(parent.section){
				def parentDays=parent.section.block.days.split(',')
				for(d in days){
					for(parentDay in parentDays){
						if(parentDay==d){
							def parentPeriods=parent.section.block.periods.split(",")
							for(p in nPeriods){
								for(parentPeriod in parentPeriods){
									if(parentPeriod==p){
										return true
									}
								}
							}
						}
					}
				}
			}
			parent=parent.parent
		}
		return false
	}
	private int calculateNodeScore(Node n){
		int score=0
		if(n?.parent!=null)
			score=n.parent.score
		DesiredScheduleCourse dc=desiredCourses.find {
			it.course.identity==n.section.course.identity
		}
		if(n.section.teacher==dc.teacher){//section teacher matches preferred teacher
			score+=TEACHER_MATCH
		}

		def friends = friendsSchedules?.count{
			n.section in it.courses
		}
		score += friends*STUDENT_FRIEND_MATCH

		score+=100
		return score
	}
	private Schedule createSchedule(Node head){
		boolean validSchedule=false
		def possiblePathNodes=[]
		boolean allNodesTraversed=false
		while(!validSchedule){
			println "enter loop"
			Schedule s=new Schedule(year:criteria.year,semester:criteria.semester,student:criteria.student)
			Node bestPath=findHighestScorePath(head)//find leaf with highest score
			if(bestPath.score==-1)
				allNodesTraversed=true
			println "found best path"
			Node cursor=bestPath
			def courseSections=[]
			int courseCount=0
			while(cursor!=null){//travel up tree adding each course section
				if(cursor.section){
					courseSections.add(cursor.section)
					courseCount++
				}
				cursor=cursor.parent
			}
			if(scheduleSuggestedChanges.size()<MAX_CHANGE_SUGGESTIONS)
				scheduleSuggestedChanges.put(courseSections,new ArrayList())
			if(courseCount==criteria.courses.size()&&!allNodesTraversed){
				//found match for all courses

				//setup data structures to see if lunch period is available for each lunch day desired by student
				def lunchDayAvailible=[:]
				def availibleLunchPeriods=[]
				for(int i=criteria.student.school.lunchPeriodStart;i<=criteria.student.school.lunchPeriodEnd;i++){
					availibleLunchPeriods.add(i)
				}
				if(!criteria.lunchDesiredDays.isEmpty()){
					for(d in criteria.lunchDesiredDays.split(",")){
						def newPeriodList=[]
						for(p in availibleLunchPeriods)
							newPeriodList.add(p)
						lunchDayAvailible.put(d as int,newPeriodList)
					}
				}
				////


				for(course in courseSections){
					def days=course.block.days.split(",")
					def coursePeriods=course.block.periods.split(",")
					for(d in days){
						int dayInt=d as int
						def periods=lunchDayAvailible.get(dayInt)
						if (!periods) //no lunch days to add
							continue
						for(p in coursePeriods){
							for(int i=0;i<periods.size();i++){
								if((p as int)==periods[i]){
									periods.remove(i)
									break
								}
							}
						}
					}
					s.addToCourses(course)
				}
				validSchedule=true
				for(lunchDay in lunchDayAvailible.entrySet()){//check to see if lunch is available on each day desired
					if(lunchDay.value.isEmpty()){
						scheduleSuggestedChanges.get(courseSections)?.add("Lunch period could not be assigned on day ${lunchDay.key}")
						validSchedule=false
					}
				}
				if(validSchedule)//schedule valid return schedule
					return s
				else{//invalid schedule, invalidate current path and loop back to find next best path
					validSchedule=false
					bestPath.score=-1
					if(attemptedNodes.size()<MAX_CHANGE_SUGGESTIONS)
						attemptedNodes.add(bestPath)
					bestPath.invalidPath=true
				}
			}
			else{
				//could not fill schedule with courses
				//find which course could not be located				
				attemptedNodes.add(bestPath)
				for(int i=0;i<attemptedNodes.size()&&i<MAX_CHANGE_SUGGESTIONS;i++){
					Node n=attemptedNodes[i]
					HashMap<Course, Boolean> courseInScheduleMap=[:]
					for(c in desiredCourses){
						courseInScheduleMap.put(c.course.identity, false)
					}
					cursor=n
					int numCourses=0
					while(cursor!=null){//travel up tree adding each course section
						if(cursor.section){
							courseInScheduleMap.put(cursor.section.course.identity,true)
							numCourses++
						}
						cursor=cursor.parent
					}
					Iterator it = courseInScheduleMap.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry pair = (Map.Entry)it.next();
						if(!pair.value){
							scheduleSuggestedChanges.get(courseSections).add("Could not find suitable section for ${pair.key}")
						}
						it.remove(); // avoids a ConcurrentModificationException
					}
				}
				return null
			}
		}
		return null
	}
	
	private Node findHighestScorePath(Node node){
		int score=0;
		Node highNode=null
		if(node.nextNodes.isEmpty()){
			return node
		}
		else{
			for(n in node.nextNodes){
				Node childHigh=findHighestScorePath(n)
				if(!highNode)
					highNode=childHigh
				else{
					if(highNode.score<childHigh.score)
						highNode=childHigh
				}
			}
		}
		return highNode
	}
	private void printBFS(Node n){
		Queue<Node> q=new LinkedList<Node>()
		q.add(n)
		while(!q.isEmpty()){
			Node v=q.remove()
			println v
			for(child in v.nextNodes){
				q.add(child)
			}
		}
	}
	private class Node{
		int score
		boolean invalidPath
		Node parent
		ArrayList<Node> nextNodes
		CourseSection section
		long idGen
		public Node(){
			score=0
			invalidPath=false
			nextNodes=new ArrayList()
			idGen=new Random().nextInt(1000)
		}
		public String toString(){
			return "${idGen}: ${section} - [parent: ${parent?.idGen}] - ${score}"
		}
	}
}
